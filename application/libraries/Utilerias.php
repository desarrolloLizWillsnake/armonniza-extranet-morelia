<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilerias {

    function __construct() {
        $this->ci =& get_instance();
    }

    function enviar_correo($destinatario, $remitente, $nombre_remitente, $asunto, $mensaje) {

        // Se crea la configuracion para poder enviar el correo
        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.1and1.mx',
            'smtp_port' => 25,
            'smtp_user' => 'daniel.rodriguez@armonniza.com',
            'smtp_pass' => 'Daniel2014Rodriguez',
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );

        // Se carga la libreria junto con su configuracion
        $this->ci->load->library('email', $config);
        // Se cambian las lineas finales para evitar errores de codificacion
        $this->ci->email->set_newline("\r\n");
        // Se agrega el remitente
        $this->ci->email->from($remitente, $nombre_remitente);
        // Se agrega el destinatario
        $this->ci->email->to($destinatario);
        // Se agrega el asunto
        $this->ci->email->subject($asunto);
        // Se agrega el mensaje
        $this->ci->email->message($mensaje);

        if ($this->ci->email->send()) {
            return TRUE;
        } else {
            return FALSE;
            // echo(json_encode($this->ci->email->print_debugger()));
        }
    }
}