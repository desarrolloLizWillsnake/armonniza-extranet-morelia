<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_xmlrpc {

	function insertar_proveedor_remoto($data) {
		$this->ci =& get_instance();
        
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Insertar_Proveedor');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $data["username"],
            $data["tipo_razon_social_hidden"],
            $data["pyme"],
            $data["razon_social"],
            $data["nombre"],
            $data["apellidos"],
            $data["pais"],
            $data["estado"],
            $data["ciudad"],
            $data["codigo_postal"],
            $data["colonia"],
            $data["calle"],
            $data["numero_exterior"],
            $data["numero_interior"],
            $data["nombre_uno"],
            $data["apellido_pa_uno"],
            $data["apellido_ma_uno"],
            $data["email"],
            $data["nombre_dos"],
            $data["apellido_pa_dos"],
            $data["apellido_ma_dos"],
            $data["email_dos"],
            $data["banco"],
            $data["no_cuenta"],
            $data["clabe"],
            $data["comprobante"],
            $data["user_id"],
        );       
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
        
	}

    function actualizar_proveedor_remoto($data) {
        $this->ci =& get_instance();
        
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->ci->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->ci->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->ci->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        // $this->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->ci->xmlrpc->method('Actualizar_Proveedor');
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array(
            $data["username"],
            $data["tipo_razon_social_hidden"],
            $data["pyme"],
            $data["razon_social"],
            $data["nombre"],
            $data["apellidos"],
            $data["pais"],
            $data["estado"],
            $data["ciudad"],
            $data["codigo_postal"],
            $data["colonia"],
            $data["calle"],
            $data["numero_exterior"],
            $data["numero_interior"],
            $data["nombre_uno"],
            $data["apellido_pa_uno"],
            $data["apellido_ma_uno"],
            $data["email"],
            $data["nombre_dos"],
            $data["apellido_pa_dos"],
            $data["apellido_ma_dos"],
            $data["email_dos"],
            // $data["banco"],
            // $data["no_cuenta"],
            // $data["clabe"],
            // $data["comprobante"],
            $data["id_usuario"],
        );       
        
        // Se realiza la petición al servidor
        $this->ci->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->ci->xmlrpc->send_request()) {
            return $this->ci->xmlrpc->display_error();
        }
        // De lo contrario, se muestra la respuesta 
        else {
            return $this->ci->xmlrpc->display_response();
        }
        
    }
}