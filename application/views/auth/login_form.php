<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
    'required'	=> 'required',
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'RFC';
} else if ($login_by_username) {
	$login_label = 'RFC';
} else {
	$login_label = 'Correo Electrónico';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0',
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
);
?>
<div id="myCarousel" class="carousel slide">
  <div class="carousel-inner">
    <div class="active item">
      <div class="container">
        <div class="row">
          <div class="span6">
            <div class="carousel-caption">

                <p class="lead">Este portal ha sido diseñado especialmente para ustedes como una herramienta de información y acercamiento.
					<br><br>
					Agradecemos su interés y esperamos establecer una sociedad comercial exitosa y duradera.

				</p>
				<div class="span6">
					<img src="<?php echo base_url(); ?>assets/themes/basico/img/slide/slide1.jpg">
				</div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="item">
      <div class="container">
        <div class="row">
<!--          <div class="span6">-->
<!--            <div class="carousel-caption">-->
<!--              <h1>Example headline</h1>-->
<!--              <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>-->
<!--            </div>-->
<!--       </div>-->
      <center>    <div>
            <img src="<?php echo base_url(); ?>assets/themes/basico/img/slide/slide2.jpg">
          </div></center>
        </div>
      </div>
    </div>
  </div>

  <!-- Carousel nav -->
  <a class="carousel-control left " href="#myCarousel" data-slide="prev"><i class="icon-chevron-left"></i></a>
  <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="icon-chevron-right"></i></a>
  <!-- /.Carousel nav -->
</div>

<div class="row feature-box">
  <div class="span6">
<!--    <img src="--><?php //echo base_url(); ?><!--assets/themes/basico/img/icon2.png">-->
    <p>
      Nuestro portal se desarrolló con el objetivo de establecer canales de comunicación eficientes, acceso a herramientas de comercio electrónico y registro para nuevos proveedores. 
    </p>
    <?php if ($this->config->item('allow_registration', 'tank_auth')) echo anchor('/auth/register/', 'Registrarme', ["class" => "btn btn-large btn-primary"]); ?>
  </div>

  <div class="span6">
<!--    <img src="--><?php //echo base_url(); ?><!--assets/themes/basico/img/icon1.png">-->
    <!-- <h2>Feature B</h2> -->
    <p>
      Si usted ya es proveedor ingrese con su perfil aquí:
    </p>
    <?php echo form_open($this->uri->uri_string()); ?>
    <table>
    	<tr>
			<td><?php echo form_label($login_label, $login['id']); ?></td>
			<td><?php echo form_input($login); ?></td>
			<td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>
		</tr>
		<tr>
			<td><?php echo form_label('Contraseña', $password['id']); ?></td>
			<td><?php echo form_password($password); ?></td>
			<td style="color: red;"><?php echo form_error($password['name']); ?><?php echo isset($errors[$password['name']])?$errors[$password['name']]:''; ?></td>
		</tr>
		<?php if ($show_captcha) {
			if ($use_recaptcha) { ?>
		<tr>
			<td colspan="2">
				<div id="recaptcha_image"></div>
			</td>
			<td>
				<a href="javascript:Recaptcha.reload()">Generar otro CAPTCHA</a>
				<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Generar CAPTCHA de audio</a></div>
				<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Generar CAPTCHA de imagen</a></div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="recaptcha_only_if_image">Inserte las letras superiores</div>
				<div class="recaptcha_only_if_audio">Inserte los número que escuche</div>
			</td>
			<td><input type="text" id="recaptcha_response_field" name="recaptcha_response_field" /></td>
			<td style="color: red;"><?php echo form_error('recaptcha_response_field'); ?></td>
			<?php echo $recaptcha_html; ?>
		</tr>
		<?php } else { ?>
		<tr>
			<td colspan="3">
				<p>Introduzca el código exactamente como aparece:</p>
				<?php echo $captcha_html; ?>
			</td>
		</tr>
		<tr>
			<td><?php echo form_label('Confirmation Code', $captcha['id']); ?></td>
			<td><?php echo form_input($captcha); ?></td>
			<td style="color: red;"><?php echo form_error($captcha['name']); ?></td>
		</tr>
		<?php }
		} ?>

		<tr>
			<td colspan="3">
				<?php echo form_checkbox($remember); ?>
				<?php echo form_label('Recordarme', $remember['id']); ?>
				<?php echo anchor('/auth/forgot_password/', 'Olvidé mi contraseña'); ?>
			</td>
		</tr>
	</table>

	<?php echo form_submit('submit', 'Iniciar sesión', ["class" => "btn btn-large btn-primary"]); ?>
	<?php echo form_close(); ?>
  </div>
</div>