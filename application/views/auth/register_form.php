<?php
if ($use_username) {
	$username = array(
		'name'	=> 'username',
		'id'	=> 'username',
        'class'	=> 'form-control',
        'placeholder'	=> 'RFC',
		'value' => set_value('username'),
		'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
        'required'	=> TRUE,
        'style'	=> 'text-transform:uppercase',
	);
}
$razon_social_hidden = array(
    'tipo_razon_social_hidden'  => '',
);

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
    'class'	=> 'form-control',
	'value'	=> set_value('email'),
    'required'	=> TRUE,
);
$email_dos = array(
	'name'	=> 'email_dos',
	'id'	=> 'email_dos',
    'class'	=> 'form-control',
	'value'	=> set_value('email_dos'),
    'required'	=> TRUE,
);
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
    'class'	=> 'form-control',
	'value' => set_value('password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    'required'	=> TRUE,
);
$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
    'class'	=> 'form-control',
	'value' => set_value('confirm_password'),
	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
    'required'	=> TRUE,
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
    'class'	=> 'form-control',
    'required'	=> TRUE,
);
$razon_social = array(
    'name'	=> 'razon_social',
    'id'	=> 'razon_social',
    'class'	=> 'form-control',
    'value' => set_value('razon_social'),
    'required'	=> TRUE,
);
$nombre = array(
    'name'	=> 'nombre',
    'id'	=> 'nombre',
    'class'	=> 'form-control',
    'value' => set_value('nombre'),
    'required'	=> TRUE,
);
$apellidos = array(
    'name'	=> 'apellidos',
    'id'	=> 'apellidos',
    'class'	=> 'form-control',
    'value' => set_value('apellidos'),
    'required'	=> TRUE,
);
$calle = array(
    'name'	=> 'calle',
    'id'	=> 'calle',
    'class'	=> 'form-control',
    'value' => set_value('calle'),
    'required'	=> TRUE,
);
$numero_exterior = array(
    'name'	=> 'numero_exterior',
    'id'	=> 'numero_exterior',
    'class'	=> 'form-control',
    'value' => set_value('numero_exterior'),
    'required'	=> TRUE,
);
$numero_interior = array(
    'name'	=> 'numero_interior',
    'id'	=> 'numero_interior',
    'class'	=> 'form-control',
    'value' => set_value('numero_interior'),
);
$codigo_postal = array(
    'name'  => 'codigo_postal',
    'id'    => 'codigo_postal',
    'class' => 'form-control',
    'value' => set_value('codigo_postal'),
    'required'	=> TRUE,
);
$colonia = array(
    'name'  => 'colonia',
    'id'    => 'colonia',
    'value' => set_value('colonia'),
    'required'  => TRUE,
);
$ciudad = array(
    'name'	=> 'ciudad',
    'id'	=> 'ciudad',
    'class' => 'form-control',
    'value' => set_value('ciudad'),
    'required'	=> TRUE,
);
$estado = array(
    'name'	=> 'estado',
    'id'	=> 'estado',
    'class'	=> 'form-control',
    'value' => set_value('estado'),
    'required'	=> TRUE,
);
$pais = array(
    'name'	=> 'pais',
    'id'	=> 'pais',
    'class' => 'form-control',
    'value' => set_value('pais'),
    'required'	=> TRUE,
);
$nombre_uno = array(
    'name'	=> 'nombre_uno',
    'id'	=> 'nombre_uno',
    'class'	=> 'form-control',
    'value' => set_value('nombre_uno'),
    'required'	=> TRUE,
);
$apellido_pa_uno = array(
    'name'	=> 'apellido_pa_uno',
    'id'	=> 'apellido_pa_uno',
    'class'	=> 'form-control',
    'value' => set_value('apellido_pa_uno'),
    'required'	=> TRUE,
);
$apellido_ma_uno = array(
    'name'	=> 'apellido_ma_uno',
    'id'	=> 'apellido_ma_uno',
    'class'	=> 'form-control',
    'value' => set_value('apellido_ma_uno'),
);
$telefono_uno = array(
    'name'  => 'telefono_uno',
    'id'    => 'telefono_uno',
    'class' => 'form-control',
    'value' => set_value('telefono_uno'),
    'required' => 'required',
);
$nombre_dos = array(
    'name'	=> 'nombre_dos',
    'id'	=> 'nombre_dos',
    'class'	=> 'form-control',
    'value' => set_value('nombre_dos'),
    'required'	=> TRUE,
);
$apellido_pa_dos = array(
    'name'	=> 'apellido_pa_dos',
    'id'	=> 'apellido_pa_dos',
    'class'	=> 'form-control',
    'value' => set_value('apellido_pa_dos'),
    'required'	=> TRUE,
);
$apellido_ma_dos = array(
    'name'	=> 'apellido_ma_dos',
    'id'	=> 'apellido_ma_dos',
    'class'	=> 'form-control',
    'value' => set_value('apellido_ma_dos'),
);
$telefono_dos = array(
    'name'  => 'telefono_dos',
    'id'    => 'telefono_dos',
    'class' => 'form-control',
    'value' => set_value('telefono_dos'),
    'required' => 'required',
);
$moneda = array(
    'name'	=> 'moneda',
    'id'	=> 'moneda',
    'class'	=> 'form-control',
    'value' => set_value('moneda'),
    'required'	=> TRUE,
);
$banco = array(
    'name'	=> 'banco',
    'id'	=> 'banco',
    'class'	=> 'form-control',
    'value' => set_value('banco'),
    'required'	=> TRUE,
);
$no_cuenta = array(
    'name'	=> 'no_cuenta',
    'id'	=> 'no_cuenta',
    'class'	=> 'form-control',
    'value' => set_value('no_cuenta'),
    'required'	=> TRUE,
);
$clabe = array(
    'name'	=> 'clabe',
    'id'	=> 'clabe',
    'class'	=> 'form-control',
    'maxlength'	=> 18,
    'value' => set_value('clabe'),
    'required'	=> TRUE,
);
$check_terminos = array(
    "name" => "check_terminos",
    "id" => "check_terminos",
    "value" => "acepar_terminos",
    "checked" => FALSE,
);

$comprobante = array(
    'name'        => 'archivoSubir',
    'id'          => 'archivoSubir',
    'class'       => 'upload',
    'accept'       => 'image/*, application/pdf',
    'required' => 'required',
);

?>

        
<?php echo form_open_multipart($this->uri->uri_string(), array( "id" => "forma_registro", "name" => "forma_registro", "novalidate" => "novalidate" )); ?>
    <?php if ($use_username) { ?>
        <div class="form-group">
            <?php echo form_label('RFC', $username['id']); ?>
            <?php echo form_input($username); ?>
            <br />
            <?php echo form_error($username['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
            <?php echo isset($errors[$username['name']])?$errors[$username['name']]:''; ?>
            <?php echo form_hidden($razon_social_hidden); ?>
        </div>
    <?php } ?>
    
<!--    <div class="form-group">-->
<!--        <label for="pyme">PyME</label>-->
<!--        <label class="radio-inline">-->
<!--            <input type="radio" name="pyme" id="pyme1" value="Si"> Si-->
<!--        </label>-->
<!--        <label class="radio-inline">-->
<!--            <input type="radio" name="pyme" id="pyme2" value="No" checked> No-->
<!--        </label>-->
<!--    </div>-->

    <h3 class="text-center razon_social_container" style="display: none;">Persona Moral</h3>
    
    <div class="form-group razon_social_container" style="display: none;">
        <?php echo form_label('Razón Social', $razon_social['id']); ?>
        <br />
        <?php echo form_input($razon_social); ?>
        <?php echo form_error($razon_social['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <h3 class="text-center nombre_container" style="display: none;">Persona Física</h3>
    
    <div class="form-group nombre_container" style="display: none;">
        <?php echo form_label('Nombre', $nombre['id']); ?>
        <br />
        <?php echo form_input($nombre); ?>
        <?php echo form_error($nombre['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group nombre_container" style="display: none;">
        <?php echo form_label('Apellidos', $apellidos['id']); ?>
        <br />
        <?php echo form_input($apellidos); ?>
        <?php echo form_error($apellidos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <h3 class="text-center">Domicilio Fiscal</h3>
    
    <div class="form-group">
        <?php echo form_label('País', $pais['id']); ?>
        <?php echo form_input($pais); ?>
        <br />
        <?php echo form_error($pais['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Estado', $estado['id']); ?>
        <?php echo form_input($estado); ?>
        <br />
        <?php echo form_error($estado['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Ciudad', $ciudad['id']); ?>
        <?php echo form_input($ciudad); ?>
        <br />
        <?php echo form_error($ciudad['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Código Postal', $codigo_postal['id']); ?>
        <?php echo form_input($codigo_postal); ?>
        <br />
        <?php echo form_error($codigo_postal['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Colonia', $colonia['id']); ?>
        <?php echo form_input($colonia); ?>
        <br />
        <?php echo form_error($colonia['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Calle', $calle['id']); ?>
        <?php echo form_input($calle); ?>
        <br />
        <?php echo form_error($calle['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Número Exterior', $numero_exterior['id']); ?>
        <?php echo form_input($numero_exterior); ?>
        <br />
        <?php echo form_error($numero_exterior['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Número Interior', $numero_interior['id']); ?>
        <?php echo form_input($numero_interior); ?>
        <br />
        <?php echo form_error($numero_interior['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <h3 class="text-center">Contacto Facturas</h3>
    
    <div class="form-group">
        <?php echo form_label('Nombre', $nombre_uno['id']); ?>
        <?php echo form_input($nombre_uno); ?>
        <br />
        <?php echo form_error($nombre_uno['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Paterno', $apellido_pa_uno['id']); ?>
        <?php echo form_input($apellido_pa_uno); ?>
        <br />
        <?php echo form_error($apellido_pa_uno['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Materno', $apellido_ma_uno['id']); ?>
        <?php echo form_input($apellido_ma_uno); ?>
        <br />
        <?php echo form_error($apellido_ma_uno['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Teléfono', $telefono_uno['id']); ?>
        <?php echo form_input($telefono_uno); ?>
        <br />
        <?php echo form_error($telefono_uno['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo isset($errors[$telefono_uno['name']])?$errors[$telefono_uno['name']]:''; ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Correo Electrónico', $email['id']); ?>
        <?php echo form_input($email); ?>
        <br />
        <?php echo form_error($email['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo isset($errors[$email['name']])?$errors[$email['name']]:''; ?>
    </div>
    
    <h3 class="text-center">Contacto Ventas</h3>
    
    <div class="form-group">
        <?php echo form_label('Nombre', $nombre_dos['id']); ?>
        <?php echo form_input($nombre_dos); ?>
        <br />
        <?php echo form_error($nombre_dos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Paterno', $apellido_pa_dos['id']); ?>
        <?php echo form_input($apellido_pa_dos); ?>
        <br />
        <?php echo form_error($apellido_pa_dos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Materno', $apellido_ma_dos['id']); ?>
        <?php echo form_input($apellido_ma_dos); ?>
        <br />
        <?php echo form_error($apellido_ma_dos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Teléfono', $telefono_dos['id']); ?>
        <?php echo form_input($telefono_dos); ?>
        <br />
        <?php echo form_error($telefono_dos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo isset($errors[$telefono_dos['name']])?$errors[$telefono_dos['name']]:''; ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Correo Electrónico', $email_dos['id']); ?>
        <?php echo form_input($email_dos); ?>
        <br />
        <?php echo form_error($email_dos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo isset($errors[$email_dos['name']])?$errors[$email_dos['name']]:''; ?>
    </div>

    <h3 class="text-center">Contraseña para el portal</h3>
    
    <div class="form-group">
        <?php echo form_label('Contraseña', $password['id']); ?>
        <?php echo form_password($password); ?>
        <br />
        <?php echo form_error($password['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Confirmar Contraseña', $confirm_password['id']); ?>
        <?php echo form_password($confirm_password); ?>
        <br />
        <?php echo form_error($confirm_password['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <h3 class="text-center">Datos Bancarios</h3>
    
    <!-- <div class="form-group">
        <?php echo form_label('Moneda', $moneda['id']); ?>
        <?php echo form_input($moneda); ?>
        <br />
        <?php echo form_error($moneda['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div> -->
    
    <div class="form-group">
        <?php echo form_label('Banco', $banco['id']); ?>
        <?php echo form_input($banco); ?>
        <br />
        <?php echo form_error($banco['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('No. de Cuenta', $no_cuenta['id']); ?>
        <?php echo form_input($no_cuenta); ?>
        <br />
        <?php echo form_error($no_cuenta['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('CLABE', $clabe['id']); ?>
        <?php echo form_input($clabe); ?>
        <br />
        <?php echo form_error($clabe['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Comprobante bancario', $comprobante['id']); ?>
        <p>
            Estado de cuenta bancario donde aparece el nombre del beneficiario, la cuenta y CLABE, sin importar si muestran o no saldos y movimientos.
            <br />
            El campo solo acepta archivos con formato de imagen o pdf
        </p>
        <?php echo(form_upload($comprobante)); ?>
        <br />
        <?php echo form_error($comprobante['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <iframe width="900" height="500" src="<?php echo(base_url("assets/aviso_provacidad.html")) ?>"></iframe>
    </div>
    
    <div class="form-group">
        <?php echo form_checkbox($check_terminos); ?>
        <?php echo form_label('He leído los terminos y condiciones', $check_terminos['id']); ?>
        <br />
        <?php echo form_error($check_terminos['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <?php if ($captcha_registration) {
            if ($use_recaptcha) { ?>                    
    
    <div class="form-group">
        <div id="recaptcha_image"></div>
    </div>
    
    <div class="form-group">
        <a href="javascript:Recaptcha.reload()">Generar otro CAPTCHA</a>
        <div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type('audio')">Generar CAPTCHA de audio</a></div>
        <div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type('image')">Generar CAPTCHA de imagen</a></div>
    </div>
    
    <div class="form-group">
        <div class="recaptcha_only_if_image">Inserte las letras superiores</div>
        <div class="recaptcha_only_if_audio">Inserte los número que escuche</div>
    </div>
    
    <div class="form-group">
        <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
        <br />
        <?php echo form_error('recaptcha_response_field', '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo $recaptcha_html; ?>    
    </div>
    
        <?php } else { ?>
    <div class="form-group">
        <p>Introduzca el código exactamente como aparece:</p>
        <?php echo $captcha_html; ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Código de Confirmación', $captcha['id']); ?>
        <?php echo form_input($captcha); ?>
        <br />
        <?php echo form_error($captcha['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
</tr>
        
    <?php }
    } ?>
    
    
    <?php echo form_submit('register', 'Registrarme', array(
        "disabled" => "disabled",
        "id" => "register",
        "class" => "btn btn-default")); ?>
<?php echo form_close(); ?>
        
<script type="text/javascript" src="http://api.geonames.org/export/geonamesData.js?username=willsnake"></script>