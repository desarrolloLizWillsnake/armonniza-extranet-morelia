<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="resource-type" content="document" />
    <meta name="robots" content="all, index, follow"/>
    <meta name="googlebot" content="all, index, follow" />
    <?php
      /** -- Copy from here -- */
      if(!empty($meta))
      foreach($meta as $name=>$content){
        echo "\n\t\t";
        ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
           }
      echo "\n";

      if(!empty($canonical))
      {
        echo "\n\t\t";
        ?><link rel="canonical" href="<?php echo $canonical?>" /><?php

      }
      echo "\n\t";

      foreach($css as $file){
        echo "\n\t\t";
        ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
      } echo "\n\t";

      /** -- to here -- */
    ?>
    
    <!--Font-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600' rel='stylesheet' type='text/css'>
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
      
      <!-- Fav and touch icons -->
      <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/basico/img/favicon.png" type="image/x-icon"/>
      <!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png"> -->
      <script type="text/javascript">
          var js_site_url = function( urlText ){
              var urlTmp = "<?= site_url('" + urlText + "'); ?>";
              return urlTmp;
          };
          var js_base_url = function( urlText ){
              var urlTmp = "<?= base_url('" + urlText + "'); ?>";
              return urlTmp;
          };
      </script>
      
  </head>
  <body>
  <!--HEADER ROW-->
  <div id="header-row">
    <div class="container">
      <div class="row">
              <!--LOGO-->
              <div class="span3"><a class="brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/themes/basico/img/logoSSM.png"/></a></div>
              <!-- /LOGO -->

            <!-- MAIN NAVIGATION -->  
              <div class="span9">
                <div class="navbar  pull-right">
                  <div class="navbar-inner">
                    <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
                    <div class="nav-collapse collapse navbar-responsive-collapse">
                    <ul class="nav">
                        <li class="dropdown <?php if( strpos($this->uri->uri_string(), "facturas") !== false ){ ?> active <?php }; ?>">
                          <a class="dropdown-toggle" data-toggle="dropdown">Facturas <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                  <li <?php if( strpos($this->uri->uri_string(), "facturas/subir_facturas") !== false ){ ?> class="active" <?php }; ?>><a href="<?php echo base_url(); ?>facturas/subir_facturas">Subir Facturas</a></li>
                                  <li <?php if( strpos($this->uri->uri_string(), "facturas/consultar_facturas") !== false ){ ?> class="active" <?php }; ?>><a href="<?php echo base_url(); ?>facturas/consultar_facturas">Consultar Facturas</a></li>
                            </ul>

                        </li>
                        <li <?php if( strpos($this->uri->uri_string(), "noticias") !== false ){ ?> class="active" <?php }; ?>><a href="<?php echo base_url(); ?>noticias">Noticias</a></li>
                        <li <?php if( strpos($this->uri->uri_string(), "perfil") !== false ){ ?> class="active" <?php }; ?>><a href="<?php echo base_url(); ?>perfil">Perfil</a></li>
                        <li <?php if( strpos($this->uri->uri_string(), "logout") !== false ){ ?> class="active" <?php }; ?>><a href="<?php echo base_url(); ?>auth/logout">Salir</a></li>
                    </ul>
                  </div>

                  </div>
                </div>
              </div>
            <!-- MAIN NAVIGATION -->  
      </div>
    </div>
  </div>
  <!-- /HEADER ROW -->

  


  <div class="container">
    <?php echo $output;?>
  </div>

<!--Footer
==========================-->

<footer>
    <div class="container">
      <div class="row">
      </div>
    </div>
</footer>

<!--/.Footer-->

  <?php
      foreach($js as $file){
          echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
      } echo "\n\t";
    ?>

  </body>
</html>