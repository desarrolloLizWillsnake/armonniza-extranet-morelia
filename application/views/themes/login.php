<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="">
    <meta name="resource-type" content="document" />
    <meta name="robots" content="all, index, follow"/>
    <meta name="googlebot" content="all, index, follow" />
    <?php
      /** -- Copy from here -- */
      if(!empty($meta))
      foreach($meta as $name=>$content){
        echo "\n\t\t";
        ?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
           }
      echo "\n";

      if(!empty($canonical))
      {
        echo "\n\t\t";
        ?><link rel="canonical" href="<?php echo $canonical?>" /><?php

      }
      echo "\n\t";

      foreach($css as $file){
        echo "\n\t\t";
        ?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
      } echo "\n\t";

      foreach($js as $file){
          echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
      } echo "\n\t";

      /** -- to here -- */
    ?>
    <!-- Bootstrap -->

    <link href="<?php echo base_url(); ?>assets/themes/basico/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/themes/basico/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/themes/basico/css/style.css" rel="stylesheet">
    
    <!--Font-->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600' rel='stylesheet' type='text/css'>
    
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
      
      <!-- Fav and touch icons -->
      <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/basico/img/favicon.png" type="image/x-icon"/>
      <!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
      <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png"> -->


      

    <!-- SCRIPT 
    ============================================================-->  
    <script src="<?php echo base_url(); ?>assets/themes/basico/js/jquery.js"></script>
    <script src="<?php echo base_url(); ?>assets/themes/basico/js/bootstrap.min.js"></script>
      
      
  </head>
  <body>
  <!--HEADER ROW-->
  <div id="header-row">
    <div class="container">
      <div class="row">
              <!--LOGO-->
              <div class="span3"><a class="brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/themes/basico/img/logoSSM.png"/></a></div>
              <!-- /LOGO -->
      </div>
    </div>
  </div>
  <!-- /HEADER ROW -->

  


  <div class="container">
    <?php echo $output;?>
  </div>

<!--Footer
==========================-->

<footer>
    <div class="container">
      <div class="row">
      </div>
    </div>
</footer>

<!--/.Footer-->

  <?php
      foreach($js as $file){
          echo "\n\t\t";
          ?><script src="<?php echo $file; ?>"></script><?php
      } echo "\n\t";
    ?>

  </body>
</html>