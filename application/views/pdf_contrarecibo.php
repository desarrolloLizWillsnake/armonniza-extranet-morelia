<?php
$DB1 = $this->load->database('armonniza', TRUE);

$query = $DB1->get_where('mov_contrarecibo_caratula', array('id_contrarecibo_caratula' => $contrarecibo));

$datos_compromiso = $query->row_array();

$DB1->select('*, COLUMN_JSON(nivel) AS estructura');
$DB1->where("numero_compromiso", $datos_compromiso["numero_compromiso"]);
$query_compromiso = $DB1->get('mov_compromiso_detalle');

$datos_detalle_compromiso = $query_compromiso->result_array();

?>
<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="<?= base_url("assets/themes/basico/css/bootstrap.css") ?>" type="text/css" />
		<link rel="stylesheet" href="<?= base_url("assets/themes/basico/css/bootstrap-responsive.css") ?>" type="text/css" />
		<link rel="stylesheet" href="<?= base_url("assets/themes/basico/css/style.css") ?>" type="text/css" />
		<style type="text/css">
			body {
				font-size: 2.5em;
			}
			.cont-general {
                border: 1px solid #eee;
                border-radius: 1%;
                margin: 2% 14%;
            }
            .cont-general2 {
                border: 1px solid #BDBDBD;
            }
            .cont-general .borde-inf {
                border-bottom: 1px solid #eee;
            }
            .cont-general .borde-sup {
                border-top: 1px solid #eee;
            }
            .cont-general .borde-der {
                border-right: 1px solid #eee;
            }
            .cont-general2 .borde-inf2 {
                border-bottom: 1px solid #BDBDBD;
            }
            .cont-general2 .borde-der2 {
                border-right: 1px solid #BDBDBD;
            }
		</style>
	</head>
	<body>
		<div class="container">
			<table width="100%" class="cont-general" border="0" cellspacing="3">
				<tbody>
					<tr>
						<td class="borde-inf" width="100%">
							<table>
	                			<tbody>
	                				<tr>
	                					<th align="left" width="1%"></th>
	                					<th align="left" width="30%">
	                						<img src="<?= base_url("img/logo2.png") ?>">
                						</th>
	                					<th align="left" width="16%"></th>
	                					<th align="right" width="53%">
	                						<br />
	                						<table cellspacing="6">
	                							<tbody>
		                							<tr width="150">
		                								<td>SERVICIOS DE SALUD DE MICHOACAN</td>
		            								</tr>
		            								<tr>
		            									<td>DELEGACION ADMINISTRATIVA</td>
		        									</tr>
	        									</tbody>
	    									</table>
										</th>
	            					</tr>
	        					</tbody>
	    					</table>
						</td>
					</tr>
					<br />
					<br />
					<tr>
                        <th align="center" width="100%">
                        	<strong>
                        		<h1>
                        			<big>
                        				<b>"Contra Recibo de Pago"</b>
                    				</big>
                				</h1>
            				</strong>
        				</th>
                    </tr>
					<br />
					<br />
					<br />
					<tr>
						<th align="center" width="100%" style="line-height: 9px;">
							<table cellspacing="3">
								<tbody>
									<tr>
			                            <td align="left">
			                            	<b>Fecha</b>
		                            	</td>
			                            <td align="center">
			                            	<b></b>
		                            	</td>
			                            <td align="right">
			                            	<b>No. Contra Recibo de Pago</b>
		                            	</td>
			                        </tr>
			                        <tr>
			                            <td align="left"><?= isset($fecha_emision) ? $fecha_emision : '' ?></td>
			                            <td align="center"></td>
			                            <td align="right"><?= isset($id_contrarecibo_caratula) ? $id_contrarecibo_caratula : '' ?></td>
			                        </tr>
		                        </tbody>
	                        </table>
	                    </th>
	                </tr>
	                <tr>
	                	<td class="borde-sup" width="100%">
	                		<table cellspacing="6">
	                			<tbody>
	                				<tr>
	                					<td width="1500px">
	                						<b>Proveedor o Prestador de Servicio</b>
	                						<br>
	                						<?= isset($proveedor) ? $proveedor : '' ?>
	            						</td>
	    							</tr>
<!--	                                <tr>-->
<!--	                                    <td align="left" width="50%">-->
<!--	                                    	<b>Banco</b>-->
<!--	                                    	<br>-->
<!--	                                    	--><?//= isset($banco) ? $banco : '' ?>
<!--	                                	</td>-->
<!--	                                </tr>-->
	                                <tr>
	                                    <td align="left" width="50%">
	                                    	<b>Comprobante Fiscal</b>
	                                    	<br>
	                                    	Factura
	                                	</td>
	                                </tr>
	                                <tr>
	                                    <td align="left" width="50%">
	                                    	<b>No. Comprobante Fiscal</b>
	                                    	<br>
	                                    	<?= isset($documento) ? $documento : '' ?>
	                                	</td>
	                                </tr>
	                                <tr>
	                                	<td align="left" width="50%">
	                                    	<b>Tipo de Documento</b>
	                                    	<br>
	                                    	<?= isset($tipo_documento) ? $tipo_documento : '' ?>
	                                    </td>
	                                </tr>
	                        	</tbody>
	                    	</table>
	                	</td>
	            	</tr>
	            	<tr>
	            		<td class="borde-inf borde-sup" width="100%" align="justify">
	            			<b>
	            				Observaciones
	        				</b>
	            			<br />
	            			<span>
	            				<?= isset($descripcion) ? $descripcion : '' ?>
            				</span>
	                    </td>
	                </tr>
	                <tr>
	                	<td class="borde-inf" width="100%" align="justify">
	                		<b >
	                			Documentación Anexa
	            			</b>
	                		<br />
	                		<span >
	                			<?= isset($documento) ? $documento : '' ?>
                			</span>
	                    </td>
	                </tr>

					<tr>
						<td class="borde-inf" width="100%" align="justify">
							<b >
								Subtotal
							</b>
							<br />
	                		<span >
	                			<?= isset($subtotal) ? $subtotal : '' ?>
                			</span>
						</td>
					</tr>

					<tr>
						<td class="borde-inf" width="100%" align="justify">
							<b >
								IVA
							</b>
							<br />
	                		<span >
	                			<?= isset($iva) ? $iva : '' ?>
                			</span>
						</td>
					</tr>
					<tr>
						<td class="borde-inf" width="100%" align="justify">
							<b >
								Importe Total
							</b>
							<br />
	                		<span >
	                			<?= isset($importe) ? $importe : '' ?>
                			</span>
						</td>
					</tr>



	                <tr>
	                	<td>
	                		<table style="font-size: small;" cellspacing="3">
	                			<tr>
	                				<td align="left" style="font-weight: bold;" width="25%">Estructura</td>
                            	</tr>
                            	<?php
                            		foreach($datos_detalle_compromiso as $key => $value) {
                            			$estructura = json_decode($value["estructura"], TRUE); ?>
                            			<tr>
	                            			<td align="center" width="10%"><?= $estructura["fuente_de_financiamiento"] ?></td>
	                            			<td align="center" width="10%"><?= $estructura["programa_de_financiamiento"] ?></td>
	                            			<td align="center" width="10%"><?= $estructura["centro_de_costos"] ?></td>
	                            			<td align="center" width="10%"><?= $estructura["capitulo"] ?></td>
	                            			<td align="center" width="10%"><?= $estructura["concepto"] ?></td>
	                            			<td align="center" width="10%"><?= $estructura["partida"] ?></td>
                            			</tr> <?php
                            		}
                            	?>
                        	</table>
	                	</td>
	                </tr>
	        	</tbody>
	    	</table>
		</div>
	</body>
</html>