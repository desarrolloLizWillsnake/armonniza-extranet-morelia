<div class="row">
	<div class="span12">
		<div class="page-header">
			<h1>Noticias</h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<?php
			foreach($results as $key => $value) { ?>
		
		<div class="blog-post">
			<h2><?= $value->asunto ?></h2>
			<p><?= $value->mensaje ?></p>
			<div class="postmetadata">
				<ul>
					<li>
						<i class="icon-calendar"></i><?= $value->fecha ?>
					</li>
				</ul>
			</div>
		</div>
		<?php
			}
		?>
	</div>
	<div class="pagination">
		<?= $links ?>
	</div>
</div>