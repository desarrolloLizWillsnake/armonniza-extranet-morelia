<?php
$username_input = array(
	'name'	=> 'username',
	'id'	=> 'username',
    'class'	=> 'form-control',
    'disabled'=> 'disabled',
    'placeholder'	=> 'RFC',
	'value' => isset($username) ? $username : set_value('username'),
	'maxlength'	=> $this->config->item('username_max_length', 'tank_auth'),
    'required'	=> TRUE,
    'style'	=> 'text-transform:uppercase',
);

$razon_social_hidden_input = array(
    'disabled'=> 'disabled',
    'tipo_razon_social_hidden'  => $razon_social,
);

$email_input = array(
	'name'	=> 'email',
	'id'	=> 'email',
    'class'	=> 'form-control',
	'value'	=> isset($email) ? $email : set_value('email'),
    'required'	=> TRUE,
);
$email_dos_input = array(
	'name'	=> 'email_dos',
	'id'	=> 'email_dos',
    'class'	=> 'form-control',
	'value'	=> isset($email_ventas) ? $email_ventas : set_value('email_dos'),
    'required'	=> TRUE,
);
// $password_input = array(
// 	'name'	=> 'password',
// 	'id'	=> 'password',
//     'class'	=> 'form-control',
// 	'value' => set_value('password'),
// 	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
//     'required'	=> TRUE,
// );
// $confirm_password_input = array(
// 	'name'	=> 'confirm_password',
// 	'id'	=> 'confirm_password',
//     'class'	=> 'form-control',
// 	'value' => set_value('confirm_password'),
// 	'maxlength'	=> $this->config->item('password_max_length', 'tank_auth'),
//     'required'	=> TRUE,
// );
$captcha_input = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
    'class'	=> 'form-control',
    'required'	=> TRUE,
);
$razon_social_input = array(
    'name'	=> 'razon_social',
    'disabled'=> 'disabled',
    'id'	=> 'razon_social',
    'class'	=> 'form-control',
    'value' => isset($razon_social) ? $razon_social : set_value('razon_social'),
    'required'	=> TRUE,
);
$nombre_input = array(
    'name'	=> 'nombre',
    'disabled'=> 'disabled',
    'id'	=> 'nombre',
    'class'	=> 'form-control',
    'value' => isset($nombre) ? $nombre : set_value('nombre'),
    'required'	=> TRUE,
);
$apellidos_input = array(
    'name'	=> 'apellidos',
    'disabled'=> 'disabled',
    'id'	=> 'apellidos',
    'class'	=> 'form-control',
    'value' => isset($apellidos) ? $apellidos : set_value('apellidos'),
    'required'	=> TRUE,
);
$calle_input = array(
    'name'	=> 'calle',
    'id'	=> 'calle',
    'class'	=> 'form-control',
    'value' => isset($calle) ? $calle : set_value('calle'),
    'required'	=> TRUE,
);
$numero_exterior_input = array(
    'name'	=> 'numero_exterior',
    'id'	=> 'numero_exterior',
    'class'	=> 'form-control',
    'value' => isset($numero_exterior) ? $numero_exterior : set_value('numero_exterior'),
    'required'	=> TRUE,
);
$numero_interior_input = array(
    'name'	=> 'numero_interior',
    'id'	=> 'numero_interior',
    'class'	=> 'form-control',
    'value' => isset($numero_interior) ? $numero_interior : set_value('numero_interior'),
);
$colonia_input = array(
    'name'	=> 'colonia',
    'id'	=> 'colonia',
    'value' => isset($colonia) ? $colonia : set_value('colonia'),
    'required'	=> TRUE,
);
$codigo_postal_input = array(
    'name'	=> 'codigo_postal',
    'id'	=> 'codigo_postal',
    'class'	=> 'form-control',
    'value' => isset($codigo_postal) ? $codigo_postal : set_value('codigo_postal'),
    'required'	=> TRUE,
);
$ciudad_input = array(
    'name'	=> 'ciudad',
    'id'	=> 'ciudad',
    'value' => isset($ciudad) ? $ciudad : set_value('ciudad'),
    'required'	=> TRUE,
);
$estado_input = array(
    'name'	=> 'estado',
    'id'	=> 'estado',
    'class'	=> 'form-control',
    'value' => isset($estado) ? $estado : set_value('estado'),
    'required'	=> TRUE,
);
$pais_input = array(
    'name'	=> 'pais',
    'id'	=> 'pais',
    'value' => isset($pais) ? $pais : set_value('pais'),
    'required'	=> TRUE,
);
$nombre_uno_input = array(
    'name'	=> 'nombre_uno',
    'id'	=> 'nombre_uno',
    'class'	=> 'form-control',
    'value' => isset($nombre_facturas) ? $nombre_facturas : set_value('nombre_uno'),
    'required'	=> TRUE,
);
$apellido_pa_uno_input = array(
    'name'	=> 'apellido_pa_uno',
    'id'	=> 'apellido_pa_uno',
    'class'	=> 'form-control',
    'value' => isset($apellido_paterno_facturas) ? $apellido_paterno_facturas : set_value('apellido_pa_uno'),
    'required'	=> TRUE,
);
$apellido_ma_uno_input = array(
    'name'	=> 'apellido_ma_uno',
    'id'	=> 'apellido_ma_uno',
    'class'	=> 'form-control',
    'value' => isset($apellido_materno_facturas) ? $apellido_materno_facturas : set_value('apellido_ma_uno'),
);
$telefono_uno_input = array(
    'name'  => 'telefono_uno',
    'id'    => 'telefono_uno',
    'class' => 'form-control',
    'value' => isset($telefono_facturas) ? $telefono_facturas : set_value('telefono_uno'),
);
$nombre_dos_input = array(
    'name'	=> 'nombre_dos',
    'id'	=> 'nombre_dos',
    'class'	=> 'form-control',
    'value' => isset($nombre_ventas) ? $nombre_ventas : set_value('nombre_dos'),
    'required'	=> TRUE,
);
$apellido_pa_dos_input = array(
    'name'	=> 'apellido_pa_dos',
    'id'	=> 'apellido_pa_dos',
    'class'	=> 'form-control',
    'value' => isset($apellido_paterno_ventas) ? $apellido_paterno_ventas : set_value('apellido_pa_dos'),
    'required'	=> TRUE,
);
$apellido_ma_dos_input = array(
    'name'	=> 'apellido_ma_dos',
    'id'	=> 'apellido_ma_dos',
    'class'	=> 'form-control',
    'value' => isset($apellido_materno_ventas) ? $apellido_materno_ventas : set_value('apellido_ma_dos'),
);
$telefono_dos_input = array(
    'name'  => 'telefono_dos',
    'id'    => 'telefono_dos',
    'class' => 'form-control',
    'value' => isset($telefono_ventas) ? $telefono_ventas : set_value('telefono_dos'),
);

//$moneda_input = array(
//    'name'	=> 'moneda',
//    'id'	=> 'moneda',
//    'class'	=> 'form-control',
//    'value' => isset($moneda) ? $moneda : set_value('moneda'),
//    'required'	=> TRUE,
//);
//$banco_input = array(
//    'name'	=> 'banco',
//    'id'	=> 'banco',
//    'class'	=> 'form-control',
//    'value' => isset($banco) ? $banco : set_value('banco'),
//    'required'	=> TRUE,
//);
//$no_cuenta_input = array(
//    'name'	=> 'no_cuenta',
//    'id'	=> 'no_cuenta',
//    'class'	=> 'form-control',
//    'value' => isset($no_cuenta) ? $no_cuenta : set_value('no_cuenta'),
//    'required'	=> TRUE,
//);
//$clabe_input = array(
//    'name'	=> 'clabe',
//    'id'	=> 'clabe',
//    'class'	=> 'form-control',
//    'maxlength'	=> 18,
//    'value' => isset($CLABE) ? $CLABE : set_value('clabe'),
//    'required'	=> TRUE,
//);

$comprobante_input = array(
    'name'        => 'archivoSubir',
    'id'          => 'archivoSubir',
    'class'       => 'upload',
    'accept'       => 'image/*',
    'required' => 'required',
);

?>

<div class="row">
	<div class="span12">
		<div class="page-header">
			<h1>Perfil</h1>
		</div>
	</div>
</div>

<?php if(isset($mensaje)) echo $mensaje; ?>
        
<?php echo form_open_multipart($this->uri->uri_string(), array( "id" => "forma_registro", "name" => "forma_registro", "novalidate" => "novalidate" )); ?>
    <div class="form-group">
        <?php echo form_label('RFC', $username_input['id']); ?>
        <?php echo form_input($username_input); ?>
        <br />
        <?php echo form_error($username_input['name'], '<div class="alert alert-danger" role="alert">', '</div>');  ?>
        <?php echo isset($errors[$username_input['name']])?$errors[$username_input['name']]:''; ?>
        <?php echo form_hidden($razon_social_hidden_input); ?>
    </div>



<!--    <div class="form-group">-->
<!--        <label for="pyme">PyME</label>-->
<!--        <label class="radio-inline">-->
<!--            <input type="radio" name="pyme" id="pyme1" value="Si"> Si-->
<!--        </label>-->
<!--        <label class="radio-inline">-->
<!--            <input type="radio" name="pyme" id="pyme2" value="No" checked> No-->
<!--        </label>-->
<!--    </div>-->

    <h3 class="text-center razon_social_container" style="display: none;">Persona Moral</h3>

    <div class="form-group razon_social_container" style="display: none;">
        <?php echo form_label('Razón Social', $razon_social_input['id']); ?>
        <br />
        <?php echo form_input($razon_social_input); ?>
        <?php echo form_error($razon_social_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <h3 class="text-center nombre_container" style="display: none;">Persona Física</h3>
    
    <div class="form-group nombre_container" style="display: none;">
        <?php echo form_label('Nombre', $nombre_input['id']); ?>
        <br />
        <?php echo form_input($nombre_input); ?>
        <?php echo form_error($nombre_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group nombre_container" style="display: none;">
        <?php echo form_label('Apellidos', $apellidos_input['id']); ?>
        <br />
        <?php echo form_input($apellidos_input); ?>
        <?php echo form_error($apellidos_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <h3 class="text-center">Domicilio Fiscal</h3>
    
    <div class="form-group">
        <?php echo form_label('País', $pais_input['id']); ?>
        <?php echo form_input($pais_input); ?>
        <br />
        <?php echo form_error($pais_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Estado', $estado_input['id']); ?>
        <?php echo form_input($estado_input); ?>
        <br />
        <?php echo form_error($estado_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Ciudad', $ciudad_input['id']); ?>
        <?php echo form_input($ciudad_input); ?>
        <br />
        <?php echo form_error($ciudad_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Código Postal', $codigo_postal_input['id']); ?>
        <?php echo form_input($codigo_postal_input); ?>
        <br />
        <?php echo form_error($codigo_postal_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Colonia', $colonia_input['id']); ?>
        <?php echo form_input($colonia_input); ?>
        <br />
        <?php echo form_error($colonia_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Calle', $calle_input['id']); ?>
        <?php echo form_input($calle_input); ?>
        <br />
        <?php echo form_error($calle_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Número Exterior', $numero_exterior_input['id']); ?>
        <?php echo form_input($numero_exterior_input); ?>
        <br />
        <?php echo form_error($numero_exterior_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Número Interior', $numero_interior_input['id']); ?>
        <?php echo form_input($numero_interior_input); ?>
        <br />
        <?php echo form_error($numero_interior_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <h3 class="text-center">Contacto Facturas</h3>
    
    <div class="form-group">
        <?php echo form_label('Nombre', $nombre_uno_input['id']); ?>
        <?php echo form_input($nombre_uno_input); ?>
        <br />
        <?php echo form_error($nombre_uno_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Paterno', $apellido_pa_uno_input['id']); ?>
        <?php echo form_input($apellido_pa_uno_input); ?>
        <br />
        <?php echo form_error($apellido_pa_uno_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Materno', $apellido_ma_uno_input['id']); ?>
        <?php echo form_input($apellido_ma_uno_input); ?>
        <br />
        <?php echo form_error($apellido_ma_uno_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Teléfono', $telefono_uno_input['id']); ?>
        <?php echo form_input($telefono_uno_input); ?>
        <br />
        <?php echo form_error($telefono_uno_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo isset($errors[$telefono_uno_input['name']])?$errors[$telefono_uno_input['name']]:''; ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Correo Electrónico', $email_input['id']); ?>
        <?php echo form_input($email_input); ?>
        <br />
        <?php echo form_error($email_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--        --><?php //echo isset($errors[$email_input['name']])?$errors[$email_input['name']]:''; ?>
    </div>
    
    <h3 class="text-center">Contacto Ventas</h3>
    
    <div class="form-group">
        <?php echo form_label('Nombre', $nombre_dos_input['id']); ?>
        <?php echo form_input($nombre_dos_input); ?>
        <br />
        <?php echo form_error($nombre_dos_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Paterno', $apellido_pa_dos_input['id']); ?>
        <?php echo form_input($apellido_pa_dos_input); ?>
        <br />
        <?php echo form_error($apellido_pa_dos_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Apellido Materno', $apellido_ma_dos_input['id']); ?>
        <?php echo form_input($apellido_ma_dos_input); ?>
        <br />
        <?php echo form_error($apellido_ma_dos_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>

    <div class="form-group">
        <?php echo form_label('Teléfono', $telefono_dos_input['id']); ?>
        <?php echo form_input($telefono_dos_input); ?>
        <br />
        <?php echo form_error($telefono_dos_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
        <?php echo isset($errors[$telefono_dos_input['name']])?$errors[$telefono_dos_input['name']]:''; ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Correo Electrónico', $email_dos_input['id']); ?>
        <?php echo form_input($email_dos_input); ?>
        <br />
        <?php echo form_error($email_dos_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--        --><?php //echo isset($errors[$email_dos_input['name']])?$errors[$email_dos_input['name']]:''; ?>
    </div>

    <!-- <h3 class="text-center">Contraseña para el portal</h3>
    
    <div class="form-group">
        <?php echo form_label('Contraseña', $password_input['id']); ?>
        <?php echo form_password($password_input); ?>
        <br />
        <?php echo form_error($password_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div>
    
    <div class="form-group">
        <?php echo form_label('Confirmar Contraseña', $confirm_password_input['id']); ?>
        <?php echo form_password($confirm_password_input); ?>
        <br />
        <?php echo form_error($confirm_password_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
    </div> -->
    
<!--    <h3 class="text-center">Datos Bancarios</h3>-->
<!--    -->
<!--    <!-- <div class="form-group">-->
<!--        --><?php //echo form_label('Moneda', $moneda_input['id']); ?>
<!--        --><?php //echo form_input($moneda_input); ?>
<!--        <br />-->
<!--        --><?php //echo form_error($moneda_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--    </div> -->
<!--    -->
<!--    <div class="form-group">-->
<!--        --><?php //echo form_label('Banco', $banco_input['id']); ?>
<!--        --><?php //echo form_input($banco_input); ?>
<!--        <br />-->
<!--        --><?php //echo form_error($banco_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--    </div>-->
<!--    -->
<!--    <div class="form-group">-->
<!--        --><?php //echo form_label('No. de Cuenta', $no_cuenta_input['id']); ?>
<!--        --><?php //echo form_input($no_cuenta_input); ?>
<!--        <br />-->
<!--        --><?php //echo form_error($no_cuenta_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--    </div>-->
<!--    -->
<!--    <div class="form-group">-->
<!--        --><?php //echo form_label('CLABE', $clabe_input['id']); ?>
<!--        --><?php //echo form_input($clabe_input); ?>
<!--        <br />-->
<!--        --><?php //echo form_error($clabe_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--    </div>-->
<!--    -->
<!--    <div class="form-group">-->
<!--        --><?php //echo form_label('Comprobante', $comprobante_input['id']); ?>
<!--        <img src="--><?//= $comprobante ?><!--">-->
<!--        <br />-->
<!--        <br />-->
<!--        --><?php //echo(form_upload($comprobante_input)); ?>
<!--        <br />-->
<!--        --><?php //echo form_error($comprobante_input['name'], '<div class="alert alert-danger" role="alert">', '</div>'); ?>
<!--    </div>-->
<!--    -->
    <div class="form-group">
        <?php echo form_submit('actualizar', 'Actualizar Información', array(
        "id" => "register",
        "class" => "btn btn-default")); ?>
        <a class="btn btn-large btn-primary" href="<?= base_url('auth/change_password') ?>">Cambiar Contraseña</a>
    </div>
    
<?php echo form_close(); ?>