<?php
$observaciones = array(
    'name'	=> 'observaciones',
    'id'	=> 'observaciones',
    'class'	=> 'form-control',
);
?>
<input type="hidden" value="<?php echo $usuario; ?>" name="usuario" />
<div class="row">
	<div class="span12">
		<div class="page-header">
			<h1>Subir Facturas</h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<div class="media">
			<table class="display" id="datos_tabla">
			<thead>
	            <tr>
	                <th>No. Solicitud</th>
					<th>No. Contrato</th>
					<th>No. Partida</th>
	                <th>Fecha Emitido</th>
	                <th>Total Solicitud</th>
	                <th>Total Pagado</th>
	                <th>Seleccionar</th>
	            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
		</div>
	</div>

	<form method="get" action="seleccionssm">
		Selecciona de Donde Pertenece las Facturas:
		<br />
		<br />
		<input name="intereses" type="radio" value="salud" checked/>Servicios de Salud de Michoacán
		<br />
		<input name="seguro_popular" type="radio" value="seguro_popular" />Régimen Estatal de Protección Social en Salud (Seguro Popular)
		<br />

	</form>

	<div class="span12" id="forma_subir_facturas" style="display: none;">
		<div class="media">
			<?php echo form_open_multipart("facturas/agregar_factura", ["id" => "upload_file", "class" => "load-file"]); ?>
				<div class="form-group">
					<input type="file" name="userfile[]" accept="text/xml" multiple required="required" />
k				</div>
<!--				<div class="form-group">-->
<!--			        --><?php //echo form_label('Observaciones (opcional)', $observaciones['id']); ?>
<!--			        --><?php //echo form_textarea($observaciones); ?>
<!--			    </div>-->
				<?php echo form_submit('subirArchivo', 'Subir Factura'); ?>
            <?php echo form_close(); ?>
		</div>
	</div>
</div>