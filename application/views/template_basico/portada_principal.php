<!-- <div class="row">
	<div class="span12">
		<div class="page-header">
				<h1>
				About us
			</h1>
		</div>
	</div>
</div> -->

<div class="row">
	<div class="span12">
		<h2>Mensaje a nuestros proveedores</h2>
		<!-- <img class="pull-left" alt="" src="img/img2.png" /> -->
		<p>
			Bienvenido al portal de proveedores de la Secretaria de Salud de Michoacán, nos es muy grato dirigirnos a ustedes para informarles que, como parte de nuestros procesos de innovación, mejora continua, y con el objetivo de brindarle un servicio eficiente en el trámite y pago de sus facturas, hemos implementado un nuevo servicio en línea que ponemos a su disposición.
		</p>			
		<p>
			Contamos con una nueva aplicación para nuestros proveedores que hemos llamado “Portal de Proveedores “, en donde podrá de manera automática y en línea contar con las siguientes ventajas para la gestión de sus facturas:

		</p>
		<p>
			<ul>
				<li>
					<b>Registro de Facturas electrónicas</b> En está sección podría registrar en el Portal de Proveedores las facturas que desea enviar a revisión.
				</li>
				<li>
					<b>Consultar su facturación pendiente de cobro</b> En esta sección podrá consultar las facturas que ya fueron procesadas y tienen asignada una fecha de pago, lo cual le permitirá planear su operación al conocer sus flujos futuros.
				</li>
				<li>
					<b>Consultar su historia de pagos</b> En esta sección podrá consultar las facturas que hemos pagado a su compañía en el pasado, esta historia sé ira formando con los pagos a partir de que usted quede registrado como proveedor dentro de nuestro portal, y se tendrá una visibilidad de 3 meses.  También podrá revisar las facturas procesadas y el estatus en que se encuentran.
				</li>
			</ul>
		</p>
		<p>
			El propósito de este sitio es mantener una retroalimentación con nuestros proveedores sobre el tema de facturación electrónica.
		</p>
	</div>
</div>