<div id="myCarousel" class="carousel slide">
  <div class="carousel-inner">
    <div class="active item">
      <div class="container">
        <div class="row">
          <div class="span6">
              <div class="carousel-caption">

                  <p class="lead">Este portal ha sido diseñado especialmente para ustedes como una herramienta de información y acercamiento.<BR>

                      Agradecemos su interés y esperamos establecer una sociedad comercial exitosa y duradera.</p>
              </div>
          </div>
          <div class="span6">
            <img src="<?php echo base_url(); ?>assets/themes/basico/img/slide/slide1.jpg">
          </div>
        </div>
      </div>
    </div>

    <div class="item">
      <div class="container">
        <div class="row">
<!--          <div class="span6">-->
<!--            <div class="carousel-caption">-->
<!--              <h1>Example headline</h1>-->
<!--              <p class="lead">Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>-->
<!--            </div>-->
<!--          </div>-->
          <div class="span6">
            <img src="<?php echo base_url(); ?>assets/themes/basico/img/slide/slide2.jpg">
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Carousel nav -->
  <a class="carousel-control left " href="#myCarousel" data-slide="prev"><i class="icon-chevron-left"></i></a>
  <a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="icon-chevron-right"></i></a>
  <!-- /.Carousel nav -->
</div>

<div class="row feature-box">
  <div class="span6">
<!--    <img src="--><?php //echo base_url(); ?><!--assets/themes/basico/img/icon2.png">-->
    <p>
      Nuestro portal se desarrolló con el objetivo de establecer canales de comunicación eficientes, acceso a herramientas de comercio electrónico y registro para nuevos proveedores. 
    </p>
    <a class="btn btn-large btn-primary" href="#">Registrarse</a>
  </div>

  <div class="span6">
<!--    <img src="--><?php //echo base_url(); ?><!--assets/themes/basico/img/icon1.png">-->
    <!-- <h2>Feature B</h2> -->
    <p>
      Si ya eres proveedor de Grupo Bimbo ingresa a tu perfil aquí:
    </p>
  </div>
</div>