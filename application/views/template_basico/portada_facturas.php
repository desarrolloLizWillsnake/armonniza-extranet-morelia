<div class="row">
	<div class="span12">
		<div class="page-header">
			<h1>Facturas</h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="span6">
		<div class="media">
			<a href="<?php echo base_url(); ?>facturas/subir_facturas" class="pull-left"><img src="<?php echo base_url(); ?>assets/themes/basico/img/icon-service1.png" class="media-object" alt='' /></a>
			<div class="media-body">
				<h4 class="media-heading">Subir facturas</h4> 
				<p>En este lugar puede subir sus facturas para poder comprobar sus gastos.
				</p>
				<a href="<?php echo base_url(); ?>facturas/subir_facturas" class="btn" type="button">Subir Facturas</a>
			</div>
		</div>
	</div>

	<div class="span6">
		<div class="media">
			<a href="<?php echo base_url(); ?>facturas/consultar_facturas" class="pull-left"><img src="<?php echo base_url(); ?>assets/themes/basico/img/icon-service2.png" class="media-object" alt='' /></a>
			<div class="media-body">
				<h4 class="media-heading">Consultar Facturas</h4>
				<p>En este lugar puede consultar el estado de sus facturas.
				</p>
				<a href="<?php echo base_url(); ?>facturas/consultar_facturas" class="btn" type="button">Consultar Facturas</a>
			</div>
		</div>
	</div>
</div>