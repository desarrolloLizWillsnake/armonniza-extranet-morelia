<input type="hidden" value="<?php echo $this->security->get_csrf_hash(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>" />
<div class="row">
	<div class="span12">
		<div class="page-header">
			<h1>Consultar Facturas</h1>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<div class="media">
			<table class="display" id="datos_tabla">
			<thead>
	            <tr>
	                <th>ID</th>
	                <th>Fecha de Emisión</th>
	                <th>Folio</th>
	                <th>Total</th>
	                <th>Número Pre Compromiso</th>
	                <th>Estatus</th>
	                <th>Observaciones</th>
	                <th>Descripcion</th>
	                <th>Clase</th>
                    <th>No. Contra Recibo</th>
	                <th>Acciones</th>
	                <th>Datos</th>
	            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
		</div>
	</div>
</div>


<div class="modal fade modal_ver" tabindex="-1" role="dialog" aria-labelledby="modal_ver" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa ic-modal"></i> Resumen de Factura</h4>
            </div>
            <div class="modal-body">

                <b> <h3>Datos del Emisor</h3></b>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>RFC</label>
                            <p class="form-control-static input_view" id="rfc_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Razón Social</label>
                            <p class="form-control-static input_view" id="nombre_cliente_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Exterior</label>
                            <p class="form-control-static input_view" id="no_exterior_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Interior</label>
                            <p class="form-control-static input_view" id="no_interior_emisor_ver"></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Colonia</label>
                            <p class="form-control-static input_view" id="colonia_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Municipio / Delegación</label>
                            <p class="form-control-static input_view" id="municipio_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <p class="form-control-static input_view" id="estado_emisor_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>País</label>
                            <p class="form-control-static input_view" id="pais_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Calle</label>
                            <p class="form-control-static input_view" id="calle_emisor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Régimen Fiscal</label>
                            <p class="form-control-static input_view" id="regimen_fiscal_ver"></p>
                        </div>
                    </div>
                </div>

                <hr />


                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha</label>
                            <p class="form-control-static input_view" id="fecha_emision_ver"></p>
                        </div>
                    </div>
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Concepto</label>-->
<!--                            <p class="form-control-static input_view" id="concepto_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tipo de Comprobante</label>
                            <p class="form-control-static input_view" id="tipo_comprobante_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Número de Comprobante</label>
                            <p class="form-control-static input_view" id="numero_comprobante_ver"></p>
                        </div>
                    </div>
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Folio</label>-->
<!--                            <p class="form-control-static input_view" id="folio_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Método de Pago</label>-->
<!--                            <p class="form-control-static input_view" id="metodo_pago_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>

                <div class="row">
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Número de Comprobante</label>-->
<!--                            <p class="form-control-static input_view" id="numero_comprobante_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
                    <div class="col-md-4">>
                        <div class="form-group">
                            <label>Folio</label>
                            <p class="form-control-static input_view" id="folio_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Método de Pago</label>
                            <p class="form-control-static input_view" id="metodo_pago_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>I.V.A.</label>
                            <p class="form-control-static input_view" id="iva_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tasa de I.V.A</label>
                            <p class="form-control-static input_view" id="tasa_iva_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Subtotal</label>
                            <p class="form-control-static input_view" id="subtotal_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Total</label>
                            <p class="form-control-static input_view" id="total_ver"></p>
                        </div>
                    </div>
                </div>
                
                <hr />
                <b><h3>Datos del Receptor</h3> </b>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Razón Social</label>
                        <p class="form-control-static input_view" id="nombre_receptor_ver"></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>RFC</label>
                            <p class="form-control-static input_view" id="rfc_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Exterior</label>
                            <p class="form-control-static input_view" id="no_exterior_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>No. Interior</label>
                            <p class="form-control-static input_view" id="no_interior_receptor_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Calle</label>
                            <p class="form-control-static input_view" id="calle_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Colonia</label>
                            <p class="form-control-static input_view" id="colonia_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Municipio / Delegación</label>
                            <p class="form-control-static input_view" id="municipio_receptor_ver"></p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Estado</label>
                            <p class="form-control-static input_view" id="estado_receptor_ver"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>País</label>
                            <p class="form-control-static input_view" id="pais_receptor_ver"></p>
                        </div>
                    </div>
                </div>

                <hr />

<!--                <div class="row">-->
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>No. Certificado</label>-->
<!--                            <p class="form-control-static input_view" id="no_certificado_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Certificado</label>-->
<!--                            <p class="form-control-static input_view" id="certificado_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Sello</label>-->
<!--                            <p class="form-control-static input_view" id="sello_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

                <div class="row">
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>UUID</label>-->
<!--                            <p class="form-control-static input_view" id="uuid_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>No. Certificado SAT</label>-->
<!--                            <p class="form-control-static input_view" id="no_certificado_sat_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-4">-->
<!--                        <div class="form-group">-->
<!--                            <label>Sello SAT</label>-->
<!--                            <p class="form-control-static input_view" id="sello_sat_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>Observaciones de Factura</label>-->
<!--                            <p class="form-control-static input_view" id="observaciones_proveedor_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

<!--                <div class="row">-->
<!--                    <div class="col-md-12">-->
<!--                        <div class="form-group">-->
<!--                            <label>Observaciones de SSM</label>-->
<!--                            <p class="form-control-static input_view" id="observaciones_ver"></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal_borrar" tabindex="-1" role="dialog" aria-labelledby="modal_borrar" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Borrar Factura</h4>
            </div>
            <div class="modal-body">
                <form role="form">
                    <div class="form-group">
                        <label for="message-text" class="control-label">¿Realmente desea borrar la factura seleccionada?</label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-reply ic-color"></i> Regresar</button>
                <button type="button" class="btn btn-green" data-dismiss="modal" id="elegir_borrar_factura">Aceptar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_resultado" tabindex="-1" role="dialog" aria-labelledby="modal_resultado" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="exampleModalLabel"><i class="fa fa-remove ic-modal"></i> Resultado de Operación</h4>
            </div>
            <div class="modal-body">
                <div id="resultado_mensaje"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-green" data-dismiss="modal" >Aceptar</button>
            </div>
        </div>
    </div>
</div>