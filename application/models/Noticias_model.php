<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias_model extends CI_Model {

    public function __construct() {
        parent::__construct();

        $this->load->database('armonniza');
    }

    public function contar_noticias() {
        $DB1 = $this->load->database('armonniza', TRUE);
        return $DB1->count_all("cat_noticias");
    }

    public function get_noticias($limite, $inicio) {
        $DB1 = $this->load->database('armonniza', TRUE);
        $DB1->limit($limite, $inicio);
        $DB1->order_by("id_noticia", "desc");
        $query = $DB1->get("cat_noticias");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
   }
}