<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function tomar_datos_usuario($id_usuario) {
    	$query = $this->db->select('*')
    						->join('forma_pago_usuario', 'forma_pago_usuario.id_usuario = datos_usuario.id_usuario', 'inner')
    						->join('users', 'users.id = datos_usuario.id_usuario', 'inner')
    						->where('datos_usuario.id_usuario', $id_usuario)
    						->get('datos_usuario');
        return $query->row_array();
    }
}