<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users
 *
 * This model represents user authentication data. It operates the following tables:
 * - user account data,
 * - user profiles
 *
 * @package	Tank_auth
 * @author	Ilya Konyukhov (http://konyukhov.com/soft/)
 */
class Users extends CI_Model
{
	private $table_name			= 'users';			// user accounts
	private $profile_table_name	= 'user_profiles';	// user profiles

	function __construct()
	{
		parent::__construct();

		$ci =& get_instance();
		$this->table_name			= $ci->config->item('db_table_prefix', 'tank_auth').$this->table_name;
		$this->profile_table_name	= $ci->config->item('db_table_prefix', 'tank_auth').$this->profile_table_name;
	}

	/**
	 * Get user record by Id
	 *
	 * @param	int
	 * @param	bool
	 * @return	object
	 */
	function get_user_by_id($user_id, $activated)
	{
		$this->db->where('id', $user_id);
		$this->db->where('activated', $activated ? 1 : 0);

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by login (username or email)
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_login($login)
	{
		$this->db->where('LOWER(username)=', strtolower($login));
		$this->db->or_where('LOWER(email)=', strtolower($login));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by username
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_username($username)
	{
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Get user record by email
	 *
	 * @param	string
	 * @return	object
	 */
	function get_user_by_email($email)
	{
		$this->db->where('LOWER(email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		if ($query->num_rows() == 1) return $query->row();
		return NULL;
	}

	/**
	 * Check if username available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_username_available($username)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(username)=', strtolower($username));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Check if email available for registering
	 *
	 * @param	string
	 * @return	bool
	 */
	function is_email_available($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		$this->db->or_where('LOWER(new_email)=', strtolower($email));

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 0;
	}

	/**
	 * Create new user record
	 *
	 * @param	array
	 * @param	bool
	 * @return	array
	 */
	function create_user($data, $activated = TRUE)
	{
		$data['created'] = date('Y-m-d H:i:s');
		$data['activated'] = $activated ? 1 : 0;

		if ($this->db->insert($this->table_name, $data)) {
			$user_id = $this->db->insert_id();
			if ($activated)	$this->create_profile($user_id);
			return array('user_id' => $user_id);
		}
		return NULL;
	}
    
    function create_user_data($data)
    {
        
        $this->db->trans_start();
        
        $datos_usuario = array(
//            'pyme' => $data["pyme"],
            'regimen_fiscal' => $data["tipo_razon_social_hidden"],
            'nombre' => $data["nombre"],
			'razon_social' => $data["razon_social"],
            'apellidos' => $data["apellidos"],
            'pais' => $data["pais"],
            'estado' => $data["estado"],
            'ciudad' => $data["ciudad"],
            'codigo_postal' => $data["codigo_postal"],
            'colonia' => $data["colonia"],
            'calle' => $data["calle"],
            'numero_exterior' => $data["numero_exterior"],
            'numero_interior' => $data["numero_interior"],
            'nombre_facturas' => $data["nombre_uno"],
            'apellido_paterno_facturas' => $data["apellido_pa_uno"],
            'apellido_materno_facturas' => $data["apellido_ma_uno"],
            'telefono_facturas' => $data["telefono_uno"],
            'email_facturas' => $data["email"],
            'nombre_ventas' => $data["nombre_dos"],
            'apellido_paterno_ventas' => $data["apellido_pa_dos"],
            'apellido_materno_ventas' => $data["apellido_ma_dos"],
            'telefono_ventas' => $data["telefono_dos"],
            'email_ventas' => $data["email_dos"],
            'id_usuario' => $data["user_id"],  
        );

        $this->db->insert('datos_usuario', $datos_usuario);
        
        $datos_pago = array(
            // 'moneda' => $data["moneda"],
            'banco' => $data["banco"],
            'no_cuenta' => $data["no_cuenta"],
            'CLABE' => $data["clabe"],
            'comprobante' => $data["comprobante"],
            'id_usuario' => $data["user_id"],  
        );

        $this->db->insert('forma_pago_usuario', $datos_pago);
        
        $this->db->trans_complete();
		return NULL;
    }

    function crear_datos_usuario($data)
    {
        
        $this->db->trans_start();
        
        $datos_usuario = array(
//            'pyme' => $data[2],
            'razon_social' => $data[3],
            'nombre' => $data[4],
            'apellidos' => $data[5],
            'pais' => $data[6],
            'estado' => $data[7],
            'ciudad' => $data[8],
            'codigo_postal' => $data[9],
            'colonia' => $data[10],
            'calle' => $data[11],
            'numero_exterior' => $data[12],
            'numero_interior' => $data[13],
            'nombre_facturas' => $data[14],
            'apellido_paterno_facturas' => $data[15],
            'apellido_materno_facturas' => $data[16],
            'telefono_facturas' => $data[17],
            'email_facturas' => $data[18],
            'nombre_ventas' => $data[19],
            'apellido_paterno_ventas' => $data[20],
            'apellido_materno_ventas' => $data[21],
            'telefono_ventas' => $data[22],
            'email_ventas' => $data[23],
            'id_usuario' => $data["user_id"],  
        );

        $this->db->insert('datos_usuario', $datos_usuario);
        
        $datos_pago = array(
            // 'moneda' => $data[23],
            'banco' => $data[24],
            'no_cuenta' => $data[25],
            'CLABE' => $data[26],
            'comprobante' => $data[28],
            'id_usuario' => $data["user_id"],  
        );

        $this->db->insert('forma_pago_usuario', $datos_pago);

        $datos_proveedor = array(
            'banned' => 0,
        );

        $this->db->where('id', $data["user_id"]);
        $this->db->update('users', $datos_proveedor);
        
        $this->db->trans_complete();
		return NULL;
    }

	/**
	 * Activate user if activation key is valid.
	 * Can be called for not activated users only.
	 *
	 * @param	int
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function activate_user($user_id, $activation_key, $activate_by_email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		if ($activate_by_email) {
			$this->db->where('new_email_key', $activation_key);
		} else {
			$this->db->where('new_password_key', $activation_key);
		}
		$this->db->where('activated', 0);
		$query = $this->db->get($this->table_name);

		if ($query->num_rows() == 1) {

			$this->db->set('activated', 1);
			$this->db->set('new_email_key', NULL);
			$this->db->where('id', $user_id);
			$this->db->update($this->table_name);

			$this->create_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Purge table of non-activated users
	 *
	 * @param	int
	 * @return	void
	 */
	function purge_na($expire_period = 172800)
	{
		$this->db->where('activated', 0);
		$this->db->where('UNIX_TIMESTAMP(created) <', time() - $expire_period);
		$this->db->delete($this->table_name);
	}

	/**
	 * Delete user record
	 *
	 * @param	int
	 * @return	bool
	 */
	function delete_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->delete($this->table_name);
		if ($this->db->affected_rows() > 0) {
			$this->delete_profile($user_id);
			return TRUE;
		}
		return FALSE;
	}

	/**
	 * Set new password key for user.
	 * This key can be used for authentication when resetting user's password.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function set_password_key($user_id, $new_pass_key)
	{
		$this->db->set('new_password_key', $new_pass_key);
		$this->db->set('new_password_requested', date('Y-m-d H:i:s'));
		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Check if given password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	int
	 * @return	void
	 */
	function can_reset_password($user_id, $new_pass_key, $expire_period = 900)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		$this->db->where('UNIX_TIMESTAMP(new_password_requested) >', time() - $expire_period);

		$query = $this->db->get($this->table_name);
		return $query->num_rows() == 1;
	}

	/**
	 * Change user password if password key is valid and user is authenticated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	int
	 * @return	bool
	 */
	function reset_password($user_id, $new_pass, $new_pass_key, $expire_period = 900)
	{
		$this->db->set('password', $new_pass);
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_password_key', $new_pass_key);
		$this->db->where('UNIX_TIMESTAMP(new_password_requested) >=', time() - $expire_period);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Change user password
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function change_password($user_id, $new_pass)
	{
		$this->db->set('password', $new_pass);
		$this->db->where('id', $user_id);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Set new email for user (may be activated or not).
	 * The new email cannot be used for login or notification before it is activated.
	 *
	 * @param	int
	 * @param	string
	 * @param	string
	 * @param	bool
	 * @return	bool
	 */
	function set_new_email($user_id, $new_email, $new_email_key, $activated)
	{
		$this->db->set($activated ? 'new_email' : 'email', $new_email);
		$this->db->set('new_email_key', $new_email_key);
		$this->db->where('id', $user_id);
		$this->db->where('activated', $activated ? 1 : 0);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Activate new email (replace old email with new one) if activation key is valid.
	 *
	 * @param	int
	 * @param	string
	 * @return	bool
	 */
	function activate_new_email($user_id, $new_email_key)
	{
		$this->db->set('email', 'new_email', FALSE);
		$this->db->set('new_email', NULL);
		$this->db->set('new_email_key', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('new_email_key', $new_email_key);

		$this->db->update($this->table_name);
		return $this->db->affected_rows() > 0;
	}

	/**
	 * Update user login info, such as IP-address or login time, and
	 * clear previously generated (but not activated) passwords.
	 *
	 * @param	int
	 * @param	bool
	 * @param	bool
	 * @return	void
	 */
	function update_login_info($user_id, $record_ip, $record_time)
	{
		$this->db->set('new_password_key', NULL);
		$this->db->set('new_password_requested', NULL);

		if ($record_ip)		$this->db->set('last_ip', $this->input->ip_address());
		if ($record_time)	$this->db->set('last_login', date('Y-m-d H:i:s'));

		$this->db->where('id', $user_id);
		$this->db->update($this->table_name);
	}

	/**
	 * Ban user
	 *
	 * @param	int
	 * @param	string
	 * @return	void
	 */
	function ban_user($user_id, $reason = NULL)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 1,
			'ban_reason'	=> $reason,
		));
	}

	/**
	 * Unban user
	 *
	 * @param	int
	 * @return	void
	 */
	function unban_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->update($this->table_name, array(
			'banned'		=> 0,
			'ban_reason'	=> NULL,
		));
	}

	/**
	 * Create an empty profile for a new user
	 *
	 * @param	int
	 * @return	bool
	 */
	private function create_profile($user_id)
	{
		$this->db->set('user_id', $user_id);
		return $this->db->insert($this->profile_table_name);
	}

	/**
	 * Delete user profile
	 *
	 * @param	int
	 * @return	void
	 */
	private function delete_profile($user_id)
	{
		$this->db->where('user_id', $user_id);
		$this->db->delete($this->profile_table_name);
	}
    
    function revisar_existe_usuario($usuario)
    {
        $this->db->trans_start();
        
        $this->db->select('username')
                    ->where('username', $usuario);
        $query = $this->db->get($this->table_name);
                            
        $row = $query->row_array();
        
        $this->db->trans_complete();
        
        return $row["username"];
        
    }
    
    function revisar_existe_correo($correo)
    {
        $this->db->trans_start();
        
        $this->db->select('email')
                    ->where('email', $correo);
        $query = $this->db->get($this->table_name);
                            
        $row = $query->row_array();
        
        $this->db->trans_complete();
        
        return $row["email"];
        
    }

    function update_user($data, $id_usuario)
	{
		if ($this->db->update($this->table_name, $data, array('id' => $id_usuario))) {
			return TRUE;
		}
		return NULL;
	}
    
    function update_user_data($data)
    {
        
        $this->db->trans_start();
        
        $datos_usuario = array(
            'pyme' => $data["pyme"],
            'razon_social' => $data["tipo_razon_social_hidden"],
            'nombre' => $data["nombre"],
            'apellidos' => $data["apellidos"],
            'pais' => $data["pais"],
            'estado' => $data["estado"],
            'ciudad' => $data["ciudad"],
            'codigo_postal' => $data["codigo_postal"],
            'colonia' => $data["colonia"],
            'calle' => $data["calle"],
            'numero_exterior' => $data["numero_exterior"],
            'numero_interior' => $data["numero_interior"],
            'nombre_facturas' => $data["nombre_uno"],
            'apellido_paterno_facturas' => $data["apellido_pa_uno"],
            'apellido_materno_facturas' => $data["apellido_ma_uno"],
            'telefono_facturas' => $data["telefono_uno"],
            'email_facturas' => $data["email"],
            'nombre_ventas' => $data["nombre_dos"],
            'apellido_paterno_ventas' => $data["apellido_pa_dos"],
            'apellido_materno_ventas' => $data["apellido_ma_dos"],
            'telefono_ventas' => $data["telefono_dos"],
            'email_ventas' => $data["email_dos"],
        );

        $this->db->update('datos_usuario', $datos_usuario, array('id_usuario' => $data["id_usuario"]));
        
        // $datos_pago = array(
        //     // 'moneda' => $data["moneda"],
        //     'banco' => $data["banco"],
        //     'no_cuenta' => $data["no_cuenta"],
        //     'CLABE' => $data["clabe"],
        //     'comprobante' => $data["comprobante"],
        // );

        // $this->db->update('forma_pago_usuario', $datos_pago, array('id_usuario' => $data["id_usuario"]));
        
        $this->db->trans_complete();
		return NULL;
    }
}

/* End of file users.php */
/* Location: ./application/models/auth/users.php */