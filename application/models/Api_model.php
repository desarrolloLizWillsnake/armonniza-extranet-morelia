<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function activar_proveedor($data) {

    	$this->db->trans_begin();
        
        $datos_proveedor = array(
            'banned' => 0,
        );

        $this->db->where('id', $data);
        $this->db->update('users', $datos_proveedor);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return "exito";
        }
    }

    public function desactivar_proveedor($data) {

        $this->db->trans_begin();
        
        $this->db->where('id', $data);
        $this->db->delete('users');

        $this->db->where('id_usuario', $data);
        $this->db->delete('datos_usuario');

        $this->db->where('id_usuario', $data);
        $this->db->delete('forma_pago_usuario');
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return "exito";
        }
        

    }

    public function ver_proveedor($id_proveedor) {

        $this->db->trans_begin();

        $this->db->select('username,
                            email,
                            moneda,
                            banco,
                            no_cuenta,
                            CLABE,
                            comprobante,
                            pyme,
                            razon_social,
                            nombre,
                            apellidos,
                            ciudad,
                            estado,
                            codigo_postal,
                            colonia,
                            calle,
                            numero_exterior,
                            numero_interior,
                            nombre_facturas,
                            apellido_paterno_facturas,
                            apellido_materno_facturas,
                            telefono_facturas,
                            email_facturas,
                            nombre_ventas,
                            apellido_paterno_ventas,
                            apellido_materno_ventas,
                            telefono_ventas,
                            email_ventas');
        $this->db->from('users');
        $this->db->join('forma_pago_usuario', 'forma_pago_usuario.id_usuario = users.id', 'inner');
        $this->db->join('datos_usuario', 'datos_usuario.id_usuario = users.id', 'inner');
        $this->db->where('id', $id_proveedor);
        $query = $this->db->get();
        $resultado = $query->row_array();
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return $resultado;
        }
    }

    public function ver_factura($id_factura) {

        $this->db->trans_begin();

        $this->db->select('*');
        $this->db->from('facturas_xml');
        $this->db->join('datos_usuario', 'datos_usuario.id_usuario = facturas_xml.id_usuario');
        $this->db->where('id_facturas_xml', $id_factura);
        $query = $this->db->get();
        $resultado = $query->row_array();

        $this->db->select('*');
        $this->db->from('conceptos_facturas');
        $this->db->where('id_facturas_xml', $id_factura);
        $query2 = $this->db->get();
        $resultado2 = $query2->result_array();
        $resultado["conceptos"] = array(array());

        foreach($resultado2 as $key => $value){
            foreach ($value as $key_interno => $value_interno) {
                $resultado["conceptos"][0][$key_interno."".$key] = $value_interno;
            }
        }
        $resultado["conteo"] = $key;
        $resultado["conceptos"][1] = 'struct';
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return $resultado;
        }
    }

    function rechazar_factura($data) {
        $this->db->trans_begin();
        
        $datos_factura = array(
            'observaciones' => $data[1],
            'estatus' => 6,
        );

        $this->db->where('id_facturas_xml', $data[0]);
        $this->db->update('facturas_xml', $datos_factura);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return "exito";
        }
    }

    function autorizar_factura_1($data) {
        $this->db->trans_begin();
        
        $datos_factura = array(
            'autorizado_1' => $data[1],
            'fecha_autorizado_1' => $data[2],
            'estatus' => 2,
        );

        $this->db->where('id_facturas_xml', $data[0]);
        $this->db->update('facturas_xml', $datos_factura);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return "exito";
        }
    }

    function autorizar_factura_2($data) {
        $this->db->trans_begin();
        
        $datos_factura = array(
            'autorizado_2' => $data[1],
            'fecha_autorizado_2' => $data[2],
            'contrarecibo' => $data[3],
            'estatus' => 4,
        );

        $this->db->where('id_facturas_xml', $data[0]);
        $this->db->update('facturas_xml', $datos_factura);
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return "error";
        }
        else {
            $this->db->trans_commit();
            return "exito";
        }
    }

}