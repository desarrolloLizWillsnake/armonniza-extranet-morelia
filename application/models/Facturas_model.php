<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facturas_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function datos_factura($id_factura) {
    	$query = $this->db->get_where('facturas_xml', array('id_facturas_xml' => $id_factura));
    	return $query->row_array();
    }

    function insertar_factura($datos_factura, $conceptos) {
    	$this->db->trans_begin();

    	$check_factura = "SELECT id_facturas_xml FROM facturas_xml WHERE rfc_emisor = ? AND folio = ?;";
    	$query_check = $this->db->query($check_factura, array($datos_factura["rfc_emisor"], $datos_factura["folio"]));
        $resultado_check = $query_check->row_array();

        if($resultado_check) {
        	$this->db->trans_rollback();
            return "duplicada";
        } else {
        	$this->db->insert('facturas_xml', $datos_factura);

	    	$id = $this->db->insert_id();

	    	foreach ($conceptos as $key => $value) {
	    		$datos_insertar_concepto = array(
	    			"id_facturas_xml" => $id,
	    			"cantidad" => number_format($value["cantidad"], 2, '.', ''),
	    			"descripcion" => $value["descripcion"],
	    			"importe" => number_format($value["importe"], 2, '.', ''),
	    			"unidad" => $value["unidad"],
	    			"valor_unitario" => number_format($value["valor_unitario"], 2, '.', ''),
				);

				$this->db->insert('conceptos_facturas', $datos_insertar_concepto);
	    	}

	    	if ($this->db->trans_status() === FALSE) {
	            $this->db->trans_rollback();
	            return FALSE;
	        } else {
	            $this->db->trans_commit();
	            return "exito";
	        }
        }

    }

    function datos_contrarecibo($contrarecibo) {
    	
    	$DB1 = $this->load->database('armonniza', TRUE);

        $query = $DB1->get_where('mov_contrarecibo_caratula', array('id_contrarecibo_caratula' => $contrarecibo));
    	return $query->row_array();
    }

    function borrar_factura($id_factura) {
        $this->db->trans_begin();

        $this->db->where('id_facturas_xml', $id_factura);
        $this->db->delete('facturas_xml');

        $this->db->where('id_facturas_xml', $id_factura);
        $this->db->delete('conceptos_facturas');

        $this->db->where('id_facturas_xml', $id_factura);
        $this->db->delete('contratos_facturas');

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }

    }

    function datos_precompromiso($precompromiso) {
        
        $DB1 = $this->load->database('armonniza', TRUE);

        $DB1->select_min('total');
        $query = $DB1->get_where('mov_precompromiso_caratula', array('numero_pre' => $precompromiso));
        return $query->row_array();
    }

    function sumatoria_factura($precompromiso, $id_usuario) {
        $this->db->select('SUM(total) AS total');
        $query = $this->db->get_where('facturas_xml', array('num_precompro' => $precompromiso, 'id_usuario' => $id_usuario));
        return $query->row_array();
    }

}