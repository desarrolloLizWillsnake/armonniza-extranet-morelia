<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends CI_Controller {

	function __construct() {
		parent::__construct();
		// if (!$this->tank_auth->is_logged_in()) {
		// 	redirect('/auth/login/');
		// } else {
		// 	$data['user_id']	= $this->tank_auth->get_user_id();
		// 	$data['username']	= $this->tank_auth->get_username();
		// 	$this->load->view('welcome', $data);
		// }

		$this->output->set_template('basico');

		$this->load->css('assets/themes/basico/css/bootstrap.min.css');
        $this->load->css('assets/themes/basico/css/bootstrap-responsive.css');
		$this->load->css('assets/themes/basico/css/style.css');
        $this->load->css('assets/css/font-awesome.min.css');
		$this->load->js('assets/js/jquery.min.js');
		$this->load->js('assets/js/bootstrap.min.js');
	}

	function index() {
		$this->output->set_common_meta(
			"Perfil",
			"Perfil",
			"");
		
		// $this->load->js('assets/js/jeoquery.js');
		// $this->load->js('assets/js/jsr_class.js');
		// $this->load->js('assets/js/jeoquery.js');
		$this->load->js('assets/js/login/registro.js');

		$use_username = $this->config->item('use_username', 'tank_auth');
		if ($use_username) {
			$this->form_validation->set_rules('username', 'RFC', 'trim|required|min_length['.$this->config->item('username_min_length', 'tank_auth').']|max_length['.$this->config->item('username_max_length', 'tank_auth').']|alpha_dash');
		}

		$this->form_validation->set_rules('pyme', 'PyME', 'trim|required');
        $this->form_validation->set_rules('tipo_razon_social_hidden', 'Razón Social', 'trim');
        $this->form_validation->set_rules('nombre', 'Nombre', 'trim');
        $this->form_validation->set_rules('apellidos', 'Apellidos', 'trim');
        $this->form_validation->set_rules('pais', 'País', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');
        $this->form_validation->set_rules('ciudad', 'Ciudad', 'trim|required');
        $this->form_validation->set_rules('codigo_postal', 'Código Postal', 'trim|required');
        $this->form_validation->set_rules('colonia', 'Colonia', 'trim|required');
        $this->form_validation->set_rules('calle', 'Calle', 'trim|required');
        $this->form_validation->set_rules('numero_exterior', 'Número Exterior', 'trim|required');
        $this->form_validation->set_rules('numero_interior', 'Número Interior', 'trim');
        $this->form_validation->set_rules('nombre_uno', 'Nombre Contacto Facturas', 'trim|required');
        $this->form_validation->set_rules('apellido_pa_uno', 'Apellido Paterno Contacto Facturas', 'trim|required');
        $this->form_validation->set_rules('apellido_ma_uno', 'Apellido Materno Contacto Facturas', 'trim');
        $this->form_validation->set_rules('telefono_uno', 'Teléfono Contacto Facturas', 'trim|required');
        $this->form_validation->set_rules('email', 'Correo Electrónico Contacto Facturas', 'trim|required|valid_email');
        $this->form_validation->set_rules('nombre_dos', 'Nombre Contacto Ventas', 'trim|required');
        $this->form_validation->set_rules('apellido_pa_dos', 'Apellido Paterno Contacto Ventas', 'trim|required');
        $this->form_validation->set_rules('apellido_ma_dos', 'Apellido Materno Contacto Ventas', 'trim');
        $this->form_validation->set_rules('telefono_dos', 'Teléfono Contacto Ventas', 'trim|required');
        $this->form_validation->set_rules('email_dos', 'Correo Electrónico Contacto Ventas', 'trim|required|valid_email');
		// $this->form_validation->set_rules('password', 'Contraseña', 'trim|min_length['.$this->config->item('password_min_length', 'tank_auth').']|max_length['.$this->config->item('password_max_length', 'tank_auth').']|alpha_dash');
		// $this->form_validation->set_rules('confirm_password', 'Confirmar Contraseña', 'trim|matches[password]');
        // $this->form_validation->set_rules('moneda', 'Moneda', 'trim|required');
//        $this->form_validation->set_rules('banco', 'Banco', 'trim|required');
//        $this->form_validation->set_rules('no_cuenta', 'No. de Cuenta', 'trim|required');
//        $this->form_validation->set_rules('clabe', 'CLABE', 'trim|required|min_length[18]|max_length[18]');

		$data['errors'] = array();

		$email_activation = $this->config->item('email_activation', 'tank_auth');

		if ($this->form_validation->run()) {
	        $datos_usuario = $this->input->post();
	        
	        // Se revisa si le usuario subió un archivo
	        if (isset($_FILES['archivoSubir']) && is_uploaded_file($_FILES['archivoSubir']['tmp_name'])) {
	        	// Se prepara la configuracion de la libreria "Upload"
		        $config['upload_path'] = './upload/img/';
		        $config['allowed_types'] = '*';
		        $config['remove_spaces'] = TRUE;
		        $config['encrypt_name'] = TRUE;
		        
		        // Se carga la libreria con sus configuraciones correspondientes
		        $this->load->library('upload', $config);
		        
		        // Si el server no logra subir el archivo, despliega un mensaje de error
		        if ( ! $this->upload->do_upload('archivoSubir')) {
		            // Se capturan los errores en una variable y se imprimen para debug
		            $error = array('error' => $this->upload->display_errors());
		            $this->debugeo->imprimir_pre($error);
		        }
		        else {
		            ini_set('memory_limit', '-1');
		            // Si el server logra subir el archivo, se toman los datos del archivo
		            $datos_imagen = $this->upload->data();
		            $datos_usuario["comprobante"] = base_url("upload/img/".$datos_imagen["file_name"]); 
		            // $this->debugeo->imprimir_pre($datos_imagen);
		        }
	        } else {
	        	$info = $this->perfil_model->tomar_datos_usuario($this->tank_auth->get_user_id());

	        	$datos_usuario["comprobante"] = $info["comprobante"];
	        }

	        $datos_usuario["id_usuario"] = $this->tank_auth->get_user_id();
        
			if (!is_null($data = $this->tank_auth->update_user(
					$use_username ? $this->form_validation->set_value('username') : '',
					$this->form_validation->set_value('email'),
					$this->form_validation->set_value('password'),
					$email_activation,
                    $datos_usuario))) {									// success

				if ($email_activation) {									// send "activate" email
					$data['activation_period'] = $this->config->item('email_activation_expire', 'tank_auth') / 3600;

					unset($data['password']); // Clear password (just for any case)

					$info = $this->perfil_model->tomar_datos_usuario($this->tank_auth->get_user_id());

					foreach ($info as $key => $value) {
						$datos_perfil[$key] = $value;
					}
					$datos_perfil["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Sus datos se actualizaron correctamente.</div>';
					// $this->debugeo->imprimir_pre($datos_perfil);

					$this->load->view('template_basico/perfil', $datos_perfil);
					return TRUE;

				} else {
					if ($this->config->item('email_account_details', 'tank_auth')) {	// send "welcome" email
					}
					unset($data['password']); // Clear password (just for any case)

					$info = $this->perfil_model->tomar_datos_usuario($this->tank_auth->get_user_id());
					
					foreach ($info as $key => $value) {
						$datos_perfil[$key] = $value;
					}
					$datos_perfil["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Sus datos se actualizaron correctamente.</div>';
					// $this->debugeo->imprimir_pre($datos_perfil);

					$this->load->view('template_basico/perfil', $datos_perfil);
					return TRUE;
				}
			} else {
				$errors = $this->tank_auth->get_error_message();
				foreach ($errors as $k => $v)	$data['errors'][$k] = $this->lang->line($v);
			}
		}
		$info = $this->perfil_model->tomar_datos_usuario($this->tank_auth->get_user_id());
		foreach ($info as $key => $value) {
			$datos_perfil[$key] = $value;
		}
		// $this->debugeo->imprimir_pre($data);

		$this->load->view('template_basico/perfil', $datos_perfil);
	}

	/**
	 * Create reCAPTCHA JS and non-JS HTML to verify user as a human
	 *
	 * @return	string
	 */
	function _create_recaptcha()
	{
		$this->load->helper('recaptcha');

		// Add custom theme so we can get only image
		$options = "<script>var RecaptchaOptions = {theme: 'custom', custom_theme_widget: 'recaptcha_widget'};</script>\n";

		// Get reCAPTCHA JS and non-JS HTML
		$html = recaptcha_get_html($this->config->item('recaptcha_public_key', 'tank_auth'));

		return $options.$html;
	}

}