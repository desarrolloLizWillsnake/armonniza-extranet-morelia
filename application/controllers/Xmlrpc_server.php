<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Xmlrpc_server extends CI_Controller {

        public function index() {
                $this->load->library('xmlrpc');
                $this->load->library('xmlrpcs');

                $config['functions']['Greetings'] = array('function' => 'Xmlrpc_server.process');

                $this->xmlrpcs->initialize($config);
                $this->xmlrpcs->serve();
        }


        public function process($request) {
            
            $parameters = $request->output_parameters();
            
            // if ($parameters[0] != $username || $parameters[1] != $password) {
            //     return $this->xmlrpc->send_error_message('100', 'Acceso Inválido');
            // }

            // $response = array(
            //         array(
            //                 'you_said'  => $parameters[2],
            //                 'i_respond' => 'Not bad at all.'
            //         ),
            //         'struct'
            // );
            
            $response = array(
                $parameters,
                'struct'
            );
            
            // $response = array(
            //         array(
            //                 'first_name' => array('John', 'string'),
            //                 'last_name' => array('Doe', 'string'),
            //                 'member_id' => array(123435, 'int'),
            //                 'todo_list' => array(array('clean house', 'call mom', 'water plants'), 'array'),
            //         ),
            //         'struct'
            // );

            return $this->xmlrpc->send_response($response);
        }
}
?>