<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} 
				
		$this->output->set_template('basico');

		$this->load->css('assets/themes/basico/css/bootstrap.css');
		$this->load->css('assets/themes/basico/css/bootstrap-responsive.css');
		$this->load->css('assets/themes/basico/css/style.css');
		$this->load->js('assets/js/jquery.min.js');
		$this->load->js('assets/js/bootstrap.min.js');
	}

	function index() {
		$this->output->set_common_meta(
			"Portal de Proveedores",
			"Portal de Proveedores",
			"");
		$this->load->view('template_basico/portada_principal');
	}
}