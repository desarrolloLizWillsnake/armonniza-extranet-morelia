<?php

class Api_xmlrpc extends CI_Controller {

    // Se crea el constructor para el controlador
    public function index() {
            // parent::__construct();
            
            // Se cargan las librerías que se encargan de crear el servidor XML RPC
            $this->load->library('xmlrpc');
            $this->load->library('xmlrpcs');

            // Se agregan la funciones a la configuración del servidor XML RPC
            $config['functions']['Activar_Proveedor'] = array('function' => 'Api_xmlrpc.Activar_Proveedor');
            $config['functions']['Desactivar_Proveedor'] = array('function' => 'Api_xmlrpc.Desactivar_Proveedor');
            $config['functions']['Ver_Proveedor'] = array('function' => 'Api_xmlrpc.Ver_Proveedor');
            $config['functions']['Insertar_Proveedor'] = array('function' => 'Api_xmlrpc.Insertar_Proveedor');
            $config['functions']['Ver_Factura'] = array('function' => 'Api_xmlrpc.Ver_Factura');
            $config['functions']['Autorizar_Factura_1'] = array('function' => 'Api_xmlrpc.Autorizar_Factura_1');
            $config['functions']['Autorizar_Factura_2'] = array('function' => 'Api_xmlrpc.Autorizar_Factura_2');
            $config['functions']['Rechazar_Factura'] = array('function' => 'Api_xmlrpc.Rechazar_Factura');
            

            // Se inicia el servidor
            $this->xmlrpcs->initialize($config);
            $this->xmlrpcs->serve();
    }


    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Activar_Proveedor($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        // Aquí se tienen que insertar los datos del proveedor
        $respuesta = $this->api_model->activar_proveedor($parameters[0]);

        if($respuesta == "exito") {
            $response = array(
                    array(
                            'estatus'  => 200),
                    'struct'
            );
        } else {
            $response = array(
                    array(
                            'estatus'  => 300),
                    'struct'
            );
        }

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Desactivar_Proveedor($request) {
            // Se toman los parametros enviados por el cliente
            $parameters = $request->output_parameters();

            // Aquí se tienen que insertar los datos del proveedor
            $respuesta = $this->api_model->desactivar_proveedor($parameters[0]);

            if($respuesta == "exito") {
                $response = array(
                        array(
                                'estatus'  => 200),
                        'struct'
                );
            } else {
                $response = array(
                        array(
                                'estatus'  => 300),
                        'struct'
                );
            }

            // Se envía la respueta al cliente
            return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Ver_Proveedor($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        $respuesta = $this->api_model->ver_proveedor($parameters[0]);
        $response = array();

        foreach ($respuesta as $key => $value) {
            $response[0][$key] = $value;
        }
        $response[] = 'struct';

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }

    function Insertar_Proveedor($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        $respuesta = $this->tank_auth->crear_usuario($parameters[0], $parameters[17], FALSE, $parameters);
        
        $response = array();

        foreach ($respuesta as $key => $value) {
            $response[0][$key] = $value;
        }
        $response[] = 'struct';

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Ver_Factura($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        $respuesta = $this->api_model->ver_factura($parameters[0]);
        $response = array();

        foreach ($respuesta as $key => $value) {
            $response[0][$key] = $value;
        }
        $response[] = 'struct';

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Autorizar_Factura_1($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        // Aquí se tienen que insertar los datos del proveedor
        $respuesta = $this->api_model->autorizar_factura_1($parameters);

        if($respuesta == "exito") {
            $response = array(
                    array(
                            'estatus'  => 200),
                    'struct'
            );
        } else {
            $response = array(
                    array(
                            'estatus'  => 300),
                    'struct'
            );
        }

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Autorizar_Factura_2($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        // Aquí se tienen que insertar los datos del proveedor
        $respuesta = $this->api_model->autorizar_factura_2($parameters);

        if($respuesta == "exito") {
            $response = array(
                    array(
                            'estatus'  => 200),
                    'struct'
            );
        } else {
            $response = array(
                    array(
                            'estatus'  => 300),
                    'struct'
            );
        }

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }

    /**
     * Esta función se encarga de insertar un proveedor de manera remota
     * @param array $request conjunto de datos, con la información del proveedor
     */
    function Rechazar_Factura($request) {
        // Se toman los parametros enviados por el cliente
        $parameters = $request->output_parameters();

        // Aquí se tienen que insertar los datos del proveedor
        $respuesta = $this->api_model->rechazar_factura($parameters);

        if($respuesta == "exito") {
            $response = array(
                    array(
                            'estatus'  => 200),
                    'struct'
            );
        } else {
            $response = array(
                    array(
                            'estatus'  => 300),
                    'struct'
            );
        }

        // Se envía la respueta al cliente
        return $this->xmlrpc->send_response($response);
    }
}
?>