<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Xmlrpc_client extends CI_Controller {
    
    public function index() {
        
        // Se toma la URL que se configuró para conectarse en el archivo de configuración de la API
        $server_url = $this->config->item('server_externo');
        
        // Se carga la librería de XML RPC
        $this->load->library('xmlrpc');
                    
        // Se configura la URL junto con el puerto donde está el servidor
        $this->xmlrpc->server($server_url, 80);
        
        // Esta línea se activa para el debug
        $this->xmlrpc->set_debug(TRUE);
        
        // Se indica la función remota que se encarga de insertar el proveedor
        $this->xmlrpc->method('Insertar_Proveedor');
        
        // Datos de prueba
        $data = array(
            "username" => "1234564981981",
            "tipo_razon_social_hidden" => "fisica",
            "pyme" => "No",
            "razon_social" => "",
            "nombre" => "Daniel",
            "apellidos" => "Rodríguez Monroy",
            "country" => "MX",
            "estado" => "Distrito Federal",
            "postalcode" => "09440",
            "place" => "San Andrés Tetepilco",
            "calle" => "Emilio Carranza",
            "numero_exterior" => "162",
            "numero_interior" => "",
            "nombre_uno" => "Daniel",
            "apellido_pa_uno" => "Monroy",
            "apellido_ma_uno" => "Rodríguez",
            "email" => "willsnake87@gmail.com",
            "nombre_dos" => "Daniel",
            "apellido_pa_dos" => "Monroy",
            "apellido_ma_dos" => "Rodríguez",
            "email_dos" => "willsnake87@gmail.com",
            "moneda" => "MXN",
            "banco" => "Banorte",
            "no_cuenta" => "123456789456123",
            "clabe" => "123456789456135165",
            "comprobante" => "http://10.1.10.128/upload/img/bf75b7a922f483de0b2b518191a9c4c5.jpg",
            "id_usuario" => $this->tank_auth->get_user_id(),
        );
        
        // Se toman solo los valores del arreglo de datos, quitando los índices
        $request = array_values($data);       
        
        // Se realiza la petición al servidor
        $this->xmlrpc->request($request);

        // En caso de que haya un error, se muestra en pantalla
        if ( ! $this->xmlrpc->send_request()) {
            $this->debugeo->imprimir_pre($this->xmlrpc->display_error());
        }
        // De lo contrario, se muestra la respuesta 
        else {
            $this->debugeo->imprimir_pre($this->xmlrpc->display_response());
        }
    }
    
}
?>