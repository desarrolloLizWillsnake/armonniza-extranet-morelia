<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		} 
		
		$this->load->library("pagination");

		$this->output->set_template('basico');

		$this->load->css('assets/themes/basico/css/bootstrap.css');
		$this->load->css('assets/themes/basico/css/bootstrap-responsive.css');
		$this->load->css('assets/themes/basico/css/style.css');
		$this->load->js('assets/js/jquery.min.js');
		$this->load->js('assets/js/bootstrap.min.js');
	}

	function index() {
		redirect("noticias/listado_noticias");
	}

	function listado_noticias() {
		$this->output->set_common_meta(
			"Noticias",
			"Noticias",
			"");

		$config = array();
        $config["base_url"] = base_url() . "noticias/listado_noticias";
        $config["total_rows"] = $this->noticias_model->contar_noticias();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;

        $config['full_tag_open'] = '<ul>';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = 'Primero';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['last_link'] = 'Último';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['next_link'] = '&gt;';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config['prev_link'] = '&lt;';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li><a href="#">';
        $config['cur_tag_close'] = '</a></li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);

        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data["results"] = $this->noticias_model->
            get_noticias($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();

        $this->load->view("template_basico/noticias", $data);
	}

}