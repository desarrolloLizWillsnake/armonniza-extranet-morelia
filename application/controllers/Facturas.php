<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Facturas extends CI_Controller {

	function __construct() {
		parent::__construct();
		if (!$this->tank_auth->is_logged_in()) {
			redirect('/auth/login/');
		}

		$this->output->set_template('basico');

		$this->load->css('assets/themes/basico/css/bootstrap.min.css');
        $this->load->css('assets/themes/basico/css/bootstrap-responsive.css');
		$this->load->css('assets/themes/basico/css/style.css');
        $this->load->css('assets/css/font-awesome.min.css');
		$this->load->js('assets/js/jquery.min.js');
		$this->load->js('assets/js/bootstrap.min.js');
	}

	function index() {
		$this->output->set_common_meta(
			"Facturas",
			"Facturas",
			"");
		$this->load->view('template_basico/portada_facturas');
	}

	function subir_facturas() {
		$this->output->set_common_meta(
			"Subir Facturas",
			"Subir Facturas",
			"");
		$this->load->css('assets/datatables/media/css/jquery.dataTables.min.css');
		$this->load->css('assets/datatables/extensions/Select/css/select.dataTables.min.css');
		$this->load->js('assets/datatables/media/js/jquery.dataTables.min.js');
		$this->load->js('assets/datatables/extensions/Select/js/dataTables.select.min.js');
		$this->load->js('assets/js/jquery.number.min.js');
		$this->load->js('assets/js/facturas/subirFacturas.js');
		
		$datas = array(
			'csrf_token' => $this->security->get_csrf_hash(),
			'csrf_name' => $this->security->get_csrf_token_name(),
			'usuario' => $this->tank_auth->get_username(),
		);
		$this->load->view('template_basico/subir_facturas', $datas);
	}

	function tabla_detalle_contratos() {
		$this->output->unset_template();

		$this->datatables->set_database('armonniza');

        $proveedor = $this->tank_auth->get_username();

        $DB1 = $this->load->database('armonniza', TRUE);

        $query_proveedor = $DB1->get_where('cat_proveedores', array('RFC' => $proveedor));
        $resultado_proveedor = $query_proveedor->row_array();
    
        $this->datatables->select("mov_precompromiso_detalle.numero_pre,
                                    mov_precompromiso_caratula.no_contrato,
                                    COLUMN_GET(nivel, 'partida' as char),
        							mov_precompromiso_caratula.fecha_emision,
        							importe,
                                    mov_precompromiso_caratula.total_compromiso", TRUE)
                ->from('mov_precompromiso_caratula')
                ->join('mov_precompromiso_detalle', 'mov_precompromiso_detalle.numero_pre = mov_precompromiso_caratula.numero_pre')
                ->where('id_proveedor', $resultado_proveedor["clave_prov"])
                ->where('enfirme', 1)
                ->where('firma1', 1)
                ->where('firma2', 1)
                ->where('firma3', 1)
                ->where('total !=', "total_pagado")
                ->add_column('acciones', '<span title="Seleccionar Orden de Compra"><i class="fa fa-check"></i></span>')
                ->add_column('datos', json_encode([
                                            'csrf_token' => $this->security->get_csrf_hash(),
                                            'csrf_name' => $this->security->get_csrf_token_name(),
                                           ]
                                    ));

        echo $this->datatables->generate();
	}

	function agregar_factura(){
        $respuesta["mensaje"] = '';

        $usuario = $this->tank_auth->get_user_id();
        
        $folderName = './application/uploads/'.$usuario;

        if(!file_exists($folderName)) {
            mkdir($folderName, 0777, true);
        }

        $datos_arriba = array();

        $this->load->library('upload');
        $config = array(
            'allowed_types' => 'xml',
            'upload_path'   => $folderName,
        );

        $precompro = $this->input->post("numero_compromiso");
        $intereses = $this->input->post("intereses");
        // $this->debugeo->imprimir_pre($precompro);

        $this->upload->initialize($config);
        
        $files = $_FILES;
        $file_count = count($_FILES['userfile']['name']);

        for($i = 0; $i < $file_count; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            if( ! $this->upload->do_upload('userfile')) {
                $error = $this->upload->display_errors();
                $this->debugeo->imprimir_pre($error);
                break;
            } else {
                $datos_arriba[] = $this->upload->data();
            }
        }

        try {
            
            $this->db->trans_begin();

            foreach ($datos_arriba as $key => $data) {
                if($data['file_ext'] == ".xml" || $data['file_ext'] == ".XML") {
                    $doc = new DOMDocument();
                    $doc->load( $data['full_path'] );

                    $comprobante = $doc->getElementsByTagName( "Comprobante" );

                    foreach( $comprobante as $elemento ) {
                        setlocale(LC_MONETARY, 'es_MX');
                        $fecha = $elemento->getAttribute('fecha');
                        $fecha = explode('T', $fecha, 2);
                        $nueva_fecha = explode('-', $fecha[0]);
                        $fecha_final = $nueva_fecha[0]."-" .$nueva_fecha[1]."-".$nueva_fecha[2];

                        if($elemento->getAttribute('serie')) {
                            $numero_comprobante = $elemento->getAttribute('serie').$elemento->getAttribute('folio');
                        } else {
                            $numero_comprobante = NULL;
                        }

                        $total = $elemento->getAttribute('total');

                        if($elemento->getAttribute('Moneda')){
                            $moneda= $elemento->getAttribute('Moneda');
                        } else {
                            $moneda = NULL;
                        }

                        $subtotal= $elemento->getAttribute('subTotal');
                        $no_certificado= $elemento->getAttribute('noCertificado');
                        $certificado= $elemento->getAttribute('certificado');
                        $tipo_comprobante= $elemento->getAttribute('tipoDeComprobante');
                        $folio= $elemento->getAttribute('folio');
                        $metodoPago= $elemento->getAttribute('metodoDePago');
                        $total_formato = number_format($total,2,".",",");

                        $emisor = $doc->getElementsByTagName( "Emisor" );
                        $rfc_emisor = $emisor->item(0)->getAttribute('rfc');

                        if($rfc_emisor != strtoupper($this->tank_auth->get_username())){
                            throw new Exception("El RFC del emisor dentro de la factura ".$data['file_name'].", no corresponde a su RFC");
                        }
                        
                        if($emisor->item(0)->getAttribute('nombre')){
                            $cliente = $emisor->item(0)->getAttribute('nombre');
                        } else {
                            $cliente = NULL;
                        }

                        $iva = $doc->getElementsByTagName( "Traslado" );

                        if($iva->length != 0){
                            $valor_iva = $iva->item(0)->getAttribute('importe');
                            $tasa_iva = $iva->item(0)->getAttribute('tasa');
                        } else {
                            $valor_iva = NULL;
                            $tasa_iva = NULL;
                        }

                        $emisor = $doc->getElementsByTagName( "DomicilioFiscal" );

                        if($emisor->length != 0) {
                            $calle_emisor = $emisor->item(0)->getAttribute('calle');
                            $no_exterior_emisor = $emisor->item(0)->getAttribute('noExterior');
                            $numero_interior_emisor = $emisor->item(0)->getAttribute('noInterior');

                            if($numero_interior_emisor) {
                                $numero_interior_emisor = $emisor->item(0)->getAttribute('noInterior');
                            } else {
                                $numero_interior_emisor = NULL;
                            }

                            $colonia_emisor = $emisor->item(0)->getAttribute('colonia');
                            $municipio_emisor = $emisor->item(0)->getAttribute('municipio');
                            $estado_emisor = $emisor->item(0)->getAttribute('estado');
                            $pais_emisor = $emisor->item(0)->getAttribute('pais');
                            $cp_emisor = $emisor->item(0)->getAttribute('codigoPostal');
                        } else {
                            $calle_emisor = NULL;
                            $no_exterior_emisor = NULL;
                            $numero_interior_emisor = NULL;
                            $colonia_emisor = NULL;
                            $municipio_emisor = NULL;
                            $estado_emisor = NULL;
                            $pais_emisor = NULL;
                            $cp_emisor = NULL;
                        }

                        $regimen = $doc->getElementsByTagName( "RegimenFiscal" );
                        $regimen_fiscal = $regimen->item(0)->getAttribute('Regimen');

                        $receptor = $doc->getElementsByTagName( "Receptor" );
                        $rfc_receptor = $receptor->item(0)->getAttribute('rfc');

                        // if($rfc_receptor != "SSM960924BP8"){
                        //     throw new Exception("El RFC del receptor dentro de la factura ".$data['file_name'].", no corresponde a la Secretaría de Salud de Morelia");
                        // }

                        if($receptor->item(0)->getAttribute('nombre')){
                            $nombre_receptor = $receptor->item(0)->getAttribute('nombre');
                        } else {
                            $nombre_receptor = NULL;
                        }

                        $datas_receptor = $doc->getElementsByTagName( "Domicilio" );

                        if($datas_receptor->length != 0) {
                            $calle_receptor = $datas_receptor->item(0)->getAttribute('calle');
                            $numero_exterior_receptor = $datas_receptor->item(0)->getAttribute('noExterior');
                            
                            if($datas_receptor->item(0)->getAttribute('noInterior')) {
                                $numero_interior_receptor = $datas_receptor->item(0)->getAttribute('noInterior');
                            } else {
                                $numero_interior_receptor = NULL;
                            }

                            $colonia_receptor = $datas_receptor->item(0)->getAttribute('colonia');
                            $municipio_receptor = $datas_receptor->item(0)->getAttribute('municipio');
                            $estado_receptor = $datas_receptor->item(0)->getAttribute('estado');
                            $pais_receptor = $datas_receptor->item(0)->getAttribute('pais');
                            $cp_receptor = $datas_receptor->item(0)->getAttribute('codigoPostal');
                        } else {
                            $calle_receptor = NULL;
                            $numero_exterior_receptor = NULL;
                            $numero_interior_receptor = NULL;
                            $colonia_receptor = NULL;
                            $municipio_receptor = NULL;
                            $estado_receptor = NULL;
                            $pais_receptor = NULL;
                            $cp_receptor = NULL;
                        }

                        $conceptos = $doc->getElementsByTagName( "Concepto" );
                        
                        $longitud = $conceptos->length;
                        $arreglo_conceptos = array();

                        for($i = 0; $i < $longitud; $i++) {
                            $arreglo_conceptos[$i]["cantidad"] = $conceptos->item($i)->getAttribute('cantidad');
                            $arreglo_conceptos[$i]["descripcion"] = $conceptos->item($i)->getAttribute('descripcion');
                            $arreglo_conceptos[$i]["importe"] = $conceptos->item($i)->getAttribute('importe');
                            $arreglo_conceptos[$i]["unidad"] = $conceptos->item($i)->getAttribute('unidad');
                            $arreglo_conceptos[$i]["valor_unitario"] = $conceptos->item($i)->getAttribute('valorUnitario');
                        }


                        $complemento = $doc->getElementsByTagName( "TimbreFiscalDigital" );
                        $selloCFD = $complemento->item(0)->getAttribute('selloCFD');

                        $uuid = $complemento->item(0)->getAttribute('UUID');

                        $noCertificadoSAT = $complemento->item(0)->getAttribute('noCertificadoSAT');

                        $selloSAT = $complemento->item(0)->getAttribute('selloSAT');

                        // Se revisa que la factura, más el total de las facturas que ya subió el proveedor del mismo precompromiso
                        // sea menor a la cantidad total por precomprometer
                        $datos_precompromiso = $this->facturas_model->datos_precompromiso($precompro);
                        $suma_facturas = $this->facturas_model->sumatoria_factura($precompro, $this->tank_auth->get_user_id());

                        $total_suma_facturas = $suma_facturas["total"] + $total;

                        if($total_suma_facturas > $datos_precompromiso["total"]) {
                            throw new Exception("La factura ".$data['file_name'].", rebasa la cantidad presupuestal aprobada por la Secretaría de Salud de Morelia,");
                        }

                        $datas_comprobante = array(
                            'id_usuario' => $this->tank_auth->get_user_id(),
                            'rfc_emisor' => $rfc_emisor,
                            'numero_ext_emisor' => $no_exterior_emisor,
                            'numero_ent_emisor' => $numero_interior_emisor,
                            'colonia_emisor' => $colonia_emisor,
                            'municipio_emisor' => $municipio_emisor,
                            'estado_emisor' => $estado_emisor,
                            'pais_emisor' => $pais_emisor,
                            'cp_emisor' => $cp_emisor,
                            'calle_emisor' => $calle_emisor,
                            'regimen_fiscal' => $regimen_fiscal,
                            'fecha_emision' => $fecha_final,
                            'tipo_comprobante' => $tipo_comprobante,
                            'numero_comprobante' => $numero_comprobante,
                            'total_formato' => $total_formato,
                            'folio' => $folio,
                            'metodo_pago' => $metodoPago,
                            'moneda' => $moneda,
                            'valor_iva' => $valor_iva,
                            'tasa_iva' => $tasa_iva,
                            'subtotal' => $subtotal,
                            'total' => $total,
                            'nombre_cliente' => $cliente,
                            'rfc_receptor' => $rfc_receptor,
                            'nombre_receptor' => $nombre_receptor,
                            'calle_receptor' => $calle_receptor,
                            'numero_ext_receptor' => $numero_exterior_receptor,
                            'numero_int_receptor' => $numero_interior_receptor,
                            'colonia_receptor' => $colonia_receptor,
                            'municipio_receptor' => $municipio_receptor,
                            'estado_receptor' => $estado_receptor,
                            'pais_receptor' => $pais_receptor,
                            'cp_receptor' => $cp_receptor,
                            'ruta_factura' => $data['full_path'],
                            'no_certificado' => $no_certificado,
                            'certificado' => $certificado,
                            'sello' => $selloCFD,
                            'uuid' => $uuid,
                            'no_certificado_sat' => $noCertificadoSAT,
                            'sello_sat' => $selloSAT,
                            'num_precompro' => $precompro,
                            'tipo_factura_salud' => $intereses,
                        );

                        $resultado_insertar = $this->facturas_model->insertar_factura($datas_comprobante, $arreglo_conceptos);

                        if($resultado_insertar == "duplicada") {
                            throw new Exception("La factura ".$data['file_name']." ya ha sido insertada previamente.");
                        } elseif (!$resultado_insertar) {
                            throw new Exception("Hubo un error al tratar de insertar la factura ".$data['file_name'].", por favor, contacte a su administrador");
                        } 
                        
                    }
                } else {
                    throw new Exception("El archivo no es un XML.");
                }
            }

            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                throw new Exception("Ha ocurrido un error al insertar las facturas");
            } else {
                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Las facturas han sido insertadas con éxito.</div>';
                $this->db->trans_commit();
            }

        } catch (Exception $e) {
            $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'.</div>';
        }

        // Se carga la vista de los mensajes con el resultado del mensaje
        $this->load->view('template_basico/resultado', $respuesta);

    }

    function borrar_factura() {
        $this->output->unset_template();

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = $this->security->xss_clean(trim($value));
        }

        $respuesta = array('mensaje' => '');

        try {

            $factura = $this->facturas_model->datos_factura($datos["factura"]);

            $resultado_borrar = $this->facturas_model->borrar_factura($datos["factura"]);

            if($resultado_borrar) {

                unlink($factura["ruta_factura"]);

                $respuesta["mensaje"] = '<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>La factura ha sido borrada de manera correcta.</div>';

            } else {
                throw new Exception("Hubo un error al borrar la factura, por favor, intentelo nuevamente.");                
            }

        } catch (Exception $e) {
            $respuesta["mensaje"] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'.$e->getMessage().'.</div>';
        }

        echo(json_encode($respuesta));

    }

    function consultar_facturas() {
        $this->output->set_common_meta(
            "Consultar Facturas",
            "Consultar Facturas",
            "");
        $this->load->css('assets/datatables/media/css/jquery.dataTables.min.css');
        $this->load->js('assets/datatables/media/js/jquery.dataTables.min.js');
        $this->load->js('assets/js/jquery.number.min.js');
        $this->load->js('assets/js/facturas/consultarFacturas.js');
        
        $datas = array(
            'csrf_token' => $this->security->get_csrf_hash(),
            'csrf_name' => $this->security->get_csrf_token_name(),
            'usuario' => $this->tank_auth->get_username(),
        );
        $this->load->view('template_basico/consultar_facturas', $datas);
    }

    function tabla_facturas_cliente() {
        $this->output->unset_template();

        $proveedor = $this->tank_auth->get_user_id();

        if(!$proveedor) {
            $proveedor = 0;
        }
    
        $this->datatables->select("id_facturas_xml,
                                    fecha_emision,
                                    folio,
                                    total,
                                    num_precompro,
                                    estatus,
                                    observaciones,
                                    descripcion,
                                    clase,
                                    contrarecibo")

                ->from('facturas_xml')
                ->where('id_usuario', $proveedor)
                ->join('cat_estatus', 'cat_estatus.id_estatus = facturas_xml.estatus', 'inner')
                ->add_column('acciones', "")
                ->add_column('datos', json_encode([
                                            'csrf_token' => $this->security->get_csrf_hash(),
                                            'csrf_name' => $this->security->get_csrf_token_name(),
                                        ]
                                    ));

        echo $this->datatables->generate();
    }

    function ver_datos_factura() {
        $this->output->unset_template();

        $datos = $this->input->post();

        foreach ($datos as $key => $value) {
            $datos[$key] = $this->security->xss_clean(trim($value));
        }

        $datos_factura = $this->facturas_model->datos_factura($datos["factura"]);

        $datos_factura["csrf_token"] = $this->security->get_csrf_hash();
        $datos_factura["csrf_name"] = $this->security->get_csrf_token_name();

        echo(json_encode($datos_factura));

    }

    function imprimir_contrarecibo($id_factura) {
        $this->output->unset_template();

        $datos_factura = $this->api_model->ver_factura($id_factura);

        // $this->debugeo->imprimir_pre($datos_factura);

        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'cm', 'Letter', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Informacion de Factura');
        $pdf->SetSubject('Informacion de Factura');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(10, 10, 10);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// -----------------------------------------------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

//        $this->debugeo->imprimir_pre($datos);
//        $this->debugeo->imprimir_pre($datos_detalle);
//        $this->debugeo->imprimir_pre($datos_beneficiario);

        $pdf->AddPage();
        $titulo = '<table style="font-size: small;" cellspacing="6"><tr width="150"><td>SERVICIOS DE SALUD DE MICHOACAN</td></tr><tr><td>DELEGACION ADMINISTRATIVA</td></tr></table>';
        $tabla_titulo = '<table>
            <tr>
                <th align="left" width="1%"></th>
                <th align="left" width="30%"><img src="'.base_url("img/logo2.png").'" /></th>
                    <th align="left" width="16%"></th>
                <th align="right" width="53%"><br />'.$titulo.'</th>
            </tr>
        </table>';

        $tabla_datos_emisor = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>RFC</b></td>
                                <td align="left" width="39%">'.strtoupper($datos_factura["rfc_emisor"]).'</td>
                                <td align="left" width="14%"><b>Razón Social</b></td>
                                <td align="left" width="34%">'.$datos_factura["razon_social"].' '.$datos_factura["nombre"].' '.$datos_factura["apellidos"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Dirección</b></td>
                                <td align="left" width="39%">'.$datos_factura["calle_emisor"].' '.$datos_factura["numero_ext_emisor"].' '.$datos_factura["numero_ent_emisor"].' '.$datos_factura["colonia_emisor"].' '.$datos_factura["cp_emisor"].' '.$datos_factura["municipio_emisor"].' '.$datos_factura["estado_emisor"].'</td>

                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Folio</b></td>
                                <td align="left" width="39%">'.$datos_factura["folio"].'</td>
                                <td align="left" width="14%"><b>Método de Pago</b></td>
                                <td align="left" width="34%">'.$datos_factura["metodo_pago"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Moneda</b></td>
                                <td align="left" width="90%">'.$datos_factura["moneda"].'</td>
                            </tr>
                    </table>';

        $tabla_datos_receptor = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>RFC</b></td>
                                <td align="left" width="39%">'.strtoupper($datos_factura["rfc_receptor"]).'</td>
                                <td align="left" width="14%"><b>Razón Social</b></td>
                                <td align="left" width="34%">'.$datos_factura["nombre_receptor"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Dirección</b></td>
                                <td align="left" width="39%">'.$datos_factura["calle_receptor"].' '.$datos_factura["numero_ext_receptor"].' '.$datos_factura["numero_int_receptor"].' '.$datos_factura["colonia_receptor"].' '.$datos_factura["cp_receptor"].' '.$datos_factura["municipio_receptor"].' '.$datos_factura["estado_receptor"].'</td>
                            </tr>
                    </table>';

        $tabla_datos_contrato = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>Contrato</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Cuenta</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Anexo</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Zona</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Jurisdicción</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Unidad</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Localidad</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Periodo</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Remisión</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Venta</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                    </table>';

        $tabla_datos_factura = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="100%"><b>Sello Digital CFDI</b></td>                                
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["sello"].'</td>                                
                            </tr>
                            <tr>
                                <td align="left" width="14%"><b>Sello del SAT</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["sello_sat"].'</td>                                
                            </tr>
                            <tr>
                                <td align="left" width="100%"><b>Cadena original del complemento del certificado SAT</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["certificado"].'</td>
                            </tr>
                    </table>';

        $tabla_conceptos = '<table class="cont-general2" style="font-size:6px;" cellspacing="0" cellpadding="4" >
                                <tr>
                                    <td class="borde-inf2 borde-der2" align="center" width="60%"><b>Descripción</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="10%" ><b>Cantidad</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%" ><b>Precio</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%"><b>Importe</b></td>
                                </tr>';
        for($i = 0; $i <= $datos_factura["conteo"]; $i++) {
            // $this->debugeo->imprimir_pre($datos_factura["conceptos"][$i]["descripcion".$i]);
            $tabla_conceptos .= '
                                <tr>
                                    <td class="borde-inf2 borde-der2" align="center" width="60%">'.$datos_factura["conceptos"][$i]["descripcion".$i].'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="10%">'.$datos_factura["conceptos"][$i]["cantidad".$i].'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%">$ '.number_format($datos_factura["conceptos"][$i]["valor_unitario".$i], 2, '.', '').'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%">$ '.number_format($datos_factura["conceptos"][$i]["importe".$i], 2, '.', '').'</td>
                                </tr>';
        }
        $tabla_conceptos .= '</table>';

        $html = '
                <style>
                    .cont-general{
                        border: 1px solid #eee;
                        border-radius: 1%;
                        margin: 2% 14%;
                    }
                    .cont-general2{
                        border: 1px solid #BDBDBD;
                    }
                    .cont-general .borde-inf{
                        border-bottom: 1px solid #eee;
                    }
                    .cont-general .borde-sup{
                        border-top: 1px solid #eee;
                    }
                    .cont-general .borde-der{
                        border-right: 1px solid #eee;
                    }
                    .cont-general2 .borde-inf2{
                        border-bottom: 1px solid #BDBDBD;
                    }
                    .cont-general2 .borde-der2{
                        border-right: 1px solid #BDBDBD;
                    }
                </style>

                <table  width="100%" class="cont-general" border="0" cellspacing="3" style="font-size: small;">
                    <tr>
                        <td class="borde-inf" width="100%">'.$tabla_titulo.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><H1><b>"Resumen de Emisión Factura Física"</b></H1></th>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Emisor</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_emisor.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Receptor</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_receptor.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Contrato</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_contrato.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos de la Factura</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_factura.'</td>
                    </tr>
                     <br>

                    <tr>
                        <td>
                            '.$tabla_conceptos.'
                        </td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Subtotal</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["subtotal"], 2, '.', '').'</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Tasa de IVA</b></td>
                        <td width="10%" align="center">'.$datos_factura["tasa_iva"].'%</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>IVA</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["valor_iva"], 2, '.', '').'</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Total</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["total"], 2, '.', '').'</td>
                    </tr>
                </table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Informacion de Factura.pdf', 'FI');

    }

    function imprimir_datos_factura($factura = NULL) {
        log_message('info', 'El usuario '.$this->tank_auth->get_username().', ha impreso la información de la factura' .$factura);

        $datos_factura = $this->api_xmlrpc->ver_datos_factura($factura);

        // $this->debugeo->imprimir_pre($datos_factura);

        $this->load->library('Pdf');

        $pdf = new Pdf('P', 'cm', 'Letter', true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Armonniza');
        $pdf->SetTitle('Informacion de Factura');
        $pdf->SetSubject('Informacion de Factura');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
//        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
//        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(10, 10, 10);
//        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// -----------------------------------------------------------------------------------------------

// set font
        $pdf->SetFont('helvetica', '', 12);

//        $this->debugeo->imprimir_pre($datos);
//        $this->debugeo->imprimir_pre($datos_detalle);
//        $this->debugeo->imprimir_pre($datos_beneficiario);

        $pdf->AddPage();
        $titulo = '<table style="font-size: small;" cellspacing="6"><tr width="150"><td>SERVICIOS DE SALUD DE MICHOACAN</td></tr><tr><td>DELEGACION ADMINISTRATIVA</td></tr></table>';
        $tabla_titulo = '<table>
            <tr>
                <th align="left" width="1%"></th>
                <th align="left" width="30%"><img src="'.base_url("img/logo2.png").'" /></th>
                    <th align="left" width="16%"></th>
                <th align="right" width="53%"><br />'.$titulo.'</th>
            </tr>
        </table>';

        $tabla_datos_emisor = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>RFC</b></td>
                                <td align="left" width="39%">'.strtoupper($datos_factura["rfc_emisor"]).'</td>
                                <td align="left" width="14%"><b>Razón Social</b></td>
                                <td align="left" width="34%">'.$datos_factura["razon_social"].' '.$datos_factura["nombre"].' '.$datos_factura["apellidos"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Dirección</b></td>
                                <td align="left" width="39%">'.$datos_factura["calle_emisor"].' '.$datos_factura["numero_ext_emisor"].' '.$datos_factura["numero_ent_emisor"].' '.$datos_factura["colonia_emisor"].' '.$datos_factura["cp_emisor"].' '.$datos_factura["municipio_emisor"].' '.$datos_factura["estado_emisor"].'</td>

                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Folio</b></td>
                                <td align="left" width="39%">'.$datos_factura["folio"].'</td>
                                <td align="left" width="14%"><b>Método de Pago</b></td>
                                <td align="left" width="34%">'.$datos_factura["metodo_pago"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Moneda</b></td>
                                <td align="left" width="90%">'.$datos_factura["moneda"].'</td>
                            </tr>
                    </table>';

        $tabla_datos_receptor = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>RFC</b></td>
                                <td align="left" width="39%">'.strtoupper($datos_factura["rfc_receptor"]).'</td>
                                <td align="left" width="14%"><b>Razón Social</b></td>
                                <td align="left" width="34%">'.$datos_factura["nombre_receptor"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Dirección</b></td>
                                <td align="left" width="39%">'.$datos_factura["calle_receptor"].' '.$datos_factura["numero_ext_receptor"].' '.$datos_factura["numero_int_receptor"].' '.$datos_factura["colonia_receptor"].' '.$datos_factura["cp_receptor"].' '.$datos_factura["municipio_receptor"].' '.$datos_factura["estado_receptor"].'</td>
                            </tr>
                    </table>';

        $tabla_datos_contrato = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="11%"><b>Contrato</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Cuenta</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Anexo</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Zona</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Jurisdicción</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Unidad</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Localidad</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Periodo</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                            <tr>
                                <td align="left" width="11%"><b>Remisión</b></td>
                                <td align="left" width="39%"></td>
                                <td align="left" width="14%"><b>Venta</b></td>
                                <td align="left" width="34%"></td>
                            </tr>
                    </table>';

        $tabla_datos_factura = '<table style="font-size: small;" cellspacing="6">
                            <tr>
                                <td align="left" width="100%"><b>Sello Digital CFDI</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["sello"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="14%"><b>Sello del SAT</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["sello_sat"].'</td>
                            </tr>
                            <tr>
                                <td align="left" width="100%"><b>Cadena original del complemento del certificado SAT</b></td>
                            </tr>
                            <tr>
                                <td align="left" style="font-size: 5px;" width="100%">'.$datos_factura["certificado"].'</td>
                            </tr>
                    </table>';

        $tabla_conceptos = '<table class="cont-general2" style="font-size:6px;" cellspacing="0" cellpadding="4" >
                                <tr>
                                    <td class="borde-inf2 borde-der2" align="center" width="60%"><b>Descripción</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="10%" ><b>Cantidad</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%" ><b>Precio</b></td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%"><b>Importe</b></td>
                                </tr>';
        for($i = 0; $i <= $datos_factura["conteo"]; $i++) {
            $tabla_conceptos .= '
                                <tr>
                                    <td class="borde-inf2 borde-der2" align="center" width="60%">'.$datos_factura["conceptos"]["descripcion".$i].'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="10%">'.$datos_factura["conceptos"]["cantidad".$i].'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%">$ '.number_format($datos_factura["conceptos"]["valor_unitario".$i], 2, '.', '').'</td>
                                    <td class="borde-inf2 borde-der2" align="center" width="15%">$ '.number_format($datos_factura["conceptos"]["importe".$i], 2, '.', '').'</td>
                                </tr>';
        }
        $tabla_conceptos .= '</table>';

        $html = '
                <style>
                    .cont-general{
                        border: 1px solid #eee;
                        border-radius: 1%;
                        margin: 2% 14%;
                    }
                    .cont-general2{
                        border: 1px solid #BDBDBD;
                    }
                    .cont-general .borde-inf{
                        border-bottom: 1px solid #eee;
                    }
                    .cont-general .borde-sup{
                        border-top: 1px solid #eee;
                    }
                    .cont-general .borde-der{
                        border-right: 1px solid #eee;
                    }
                    .cont-general2 .borde-inf2{
                        border-bottom: 1px solid #BDBDBD;
                    }
                    .cont-general2 .borde-der2{
                        border-right: 1px solid #BDBDBD;
                    }
                </style>

                <table  width="100%" class="cont-general" border="0" cellspacing="3" style="font-size: small;">
                    <tr>
                        <td class="borde-inf" width="100%">'.$tabla_titulo.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><H1><b>"Resumen de Emisión Factura Física"</b></H1></th>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Emisor</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_emisor.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Receptor</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_receptor.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos del Contrato</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_contrato.'</td>
                    </tr>
                    <tr>
                        <th align="center" width="100%"><b>Datos de la Factura</b></th>
                    </tr>
                    <tr>
                        <td class="borde-sup" width="100%">'.$tabla_datos_factura.'</td>
                    </tr>
                     <br>

                    <tr>
                        <td>
                            '.$tabla_conceptos.'
                        </td>
                    </tr>
                    <tr><td width="100%"></td></tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Subtotal</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["subtotal"], 2, '.', '').'</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Tasa de IVA</b></td>
                        <td width="10%" align="center">'.$datos_factura["tasa_iva"].'%</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>IVA</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["valor_iva"], 2, '.', '').'</td>
                    </tr>
                    <tr>
                        <td width="72%"></td>
                        <td width="15%"><b>Total</b></td>
                        <td width="10%" align="center">$ '.number_format($datos_factura["total"], 2, '.', '').'</td>
                    </tr>
                </table>';

// output the HTML content

        $pdf->writeHTML($html, false, false, true, false, 'top');

//Close and output PDF document
        $pdf->Output('Informacion de Factura.pdf', 'FI');
    }

    public function ver(){
        $data=array(
            'enlaces' => $this->datos_model->ver_tablas(),
            'dump' => 0
        );
    }

}