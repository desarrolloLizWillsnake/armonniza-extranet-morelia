<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabla_datos_usuario extends CI_Migration {

        public function up()
        {
            // Se agregó la tabla de los datos del usuario
            $sql = "CREATE TABLE IF NOT EXISTS `datos_usuario` (
                        id_datos_usuario INT UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '',
                        pyme VARCHAR(2) NOT NULL COMMENT '',
                        razon_social VARCHAR(250) NULL COMMENT '',
                        nombre VARCHAR(100) NULL COMMENT '',
                        apellidos VARCHAR(250) NULL COMMENT '',
                        ciudad VARCHAR(100) NOT NULL COMMENT '',
                        estado VARCHAR(100) NOT NULL COMMENT '',
                        codigo_postal VARCHAR(7) NOT NULL COMMENT '',
                        colonia VARCHAR(250) NOT NULL COMMENT '',
                        calle VARCHAR(250) NOT NULL COMMENT '',
                        numero_exterior VARCHAR(10) NOT NULL COMMENT '',
                        numero_interior VARCHAR(10) NULL COMMENT '',
                        nombre_facturas VARCHAR(100) NOT NULL COMMENT '',
                        apellido_paterno_facturas VARCHAR(100) NOT NULL COMMENT '',
                        apellido_materno_facturas VARCHAR(100) NOT NULL COMMENT '',
                        email_facturas VARCHAR(150) NOT NULL COMMENT '',
                        nombre_ventas VARCHAR(100) NOT NULL COMMENT '',
                        apellido_paterno_ventas VARCHAR(100) NOT NULL COMMENT '',
                        apellido_materno_ventas VARCHAR(100) NOT NULL COMMENT '',
                        email_ventas VARCHAR(150) NOT NULL COMMENT '',
                        id_usuario INT UNSIGNED NOT NULL COMMENT '',
                        usuario_insertado DATETIME NULL DEFAULT '0000-00-00 00:00:00' COMMENT '',
                        usuario_modificado TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
                    PRIMARY KEY (id_datos_usuario)  COMMENT '',
                    INDEX users_datos_usuario (id_usuario ASC)  COMMENT '')
                    ENGINE = InnoDB;";
                    
            if ($this->db->simple_query($sql)) {
                    echo("Exito al crear la tabla datos_usuario <br/>");
            }
            else {
                    echo("Error al crear la tabla datos_usuario <br/>");
            }
            
            $sql2 = "ALTER TABLE `users`
                        ADD CONSTRAINT fk_users_datos_usuario
                        FOREIGN KEY (datos_usuario_id_datos_usuario)
                        REFERENCES `datos_usuario` (id_usuario)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE);";
            if ($this->db->simple_query($sql2)) {
                    echo("Exito al crear el foreign key entre la tabla usuarios y datos_usuario <br/>");
            }
            else {
                    echo("Error al crear el foreign key entre la tabla usuarios y datos_usuario <br/>");
            }
            
            $sql3 = "ALTER TABLE `users`
                        ADD CONSTRAINT fk_users_datos_usuario
                        FOREIGN KEY (datos_usuario_id_datos_usuario)
                        REFERENCES `datos_usuario` (id_usuario)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE);";
                            // ALTER TABLE `users` ADD INDEX fk_users_datos_usuario_idx (datos_usuario_id_datos_usuario ASC);
            // if ($this->db->simple_query($sql3)) {
            //         echo("Exito al crear la tabla datos_usuario");
            // }
            // else {
            //         echo("Error al crear la tabla datos_usuario");
            // }
            
        }

        public function down()
        {
            
            // Seccion inicial del usuario, sin la tabla de datos del usuario
            $sql = "ALTER TABLE `users` DROP FOREIGN KEY fk_users_datos_usuario;
                    
                    DROP TABLE `datos_usuario`;";
            if ($this->db->simple_query($sql))
            {
                    echo "Success!";
            }
            else
            {
                    echo "Query failed!";
            }
        }
}