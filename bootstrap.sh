#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install software-properties-common python-software-properties
sudo apt-get install apache2
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant /var/www
fi
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
sudo add-apt-repository 'deb http://mariadb.mirror.anstey.ca//repo/10.0/ubuntu trusty main'
sudo add-apt-repository ppa:ondrej/php5-5.6
sudo apt-get update
sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
sudo apt-get install php5-cgi php5-mysql php5-pgsql
sudo apt-get install mariadb-server