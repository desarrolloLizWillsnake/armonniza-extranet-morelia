var datos_tabla;

$(document).ready(function() {

    var tabla_principal = $('#datos_tabla').DataTable({
        "drawCallback": function( settings ) {
            var api = this.api();
            if(typeof api !== 'undefined' || typeof api !== null) {
                var info = api.rows( {page:'current'} ).data();
                if(typeof info[0] !== 'undefined' && typeof info[0] !== null) {
                    var nuevo_csrf = JSON.parse(info[0][11]);
                    $('input[name=csrfExtranetMorelia]').val(nuevo_csrf.csrf_token);
                } else {
                    $('#forma_subir_facturas').hide();
                }
            }
        },
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "../facturas/tabla_facturas_cliente",
            "type": "POST",
            "data": function ( d ) {
                d.csrfExtranetMorelia = $('input[name=csrfExtranetMorelia]').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
        },
        "columnDefs": [{
            "targets": [ 0, 7, 8, 11 ],
            "visible": false,
            "searchable": false,
        }, {
            "targets": [ 10 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                var acciones = '<a data-toggle="modal" data-target=".modal_ver" data-tooltip="Ver Información"><i class="fa fa-eye"></i></a>';
                if(row[9] != 0 && row[9] != null) {
                    acciones += '<a href="'+js_base_url('facturas/imprimir_contrarecibo/'+row[0])+'" data-tooltip="Imprimir Contrarecibo"><i class="fa fa-print"></i></a>';
                } else if(row[5] === "1" || row[5] === "6") {
                    acciones += '<a data-toggle="modal" data-target=".modal_borrar" data-tooltip="Borrar Factura"><i class="fa fa-remove"></i></a>';
                }
                return acciones;
            }
        }, {
            "targets": [ 3 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }, {
            "targets": [ 5 ],
            "searchable": false,
            "render": function ( data, type, row ) {
                return '<button type="button" class="btn '+row[8]+'" readonly>'+row[7]+'</button>';
            }
        }]
    });
});

$('#datos_tabla tbody').on( 'click', 'tr', function () {
    datos_tabla = $('#datos_tabla').DataTable().row( this ).data();
});

$('.modal_ver').on('show.bs.modal', function () {
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../facturas/ver_datos_factura",
        data: {
            csrfExtranetMorelia: $('input[name=csrfExtranetMorelia]').val(),
            factura: datos_tabla[0]
        }
    })
        .done(function( data ) {

            $('input[name='+data.csrf_name+']').val(data.csrf_token);

            $('#rfc_emisor_ver').html(data.rfc_emisor);
            $('#no_exterior_emisor_ver').html(data.numero_ext_emisor);
            $('#no_interior_emisor_ver').html(data.numero_ent_emisor);
            $('#colonia_emisor_ver').html(data.colonia_emisor);
            $('#municipio_emisor_ver').html(data.municipio_emisor);
            $('#estado_emisor_ver').html(data.estado_emisor);
            $('#pais_emisor_ver').html(data.pais_emisor);
            $('#calle_emisor_ver').html(data.calle_emisor);
            $('#regimen_fiscal_ver').html(data.regimen_fiscal);

            $('#nombre_receptor_ver').html(data.nombre_receptor);
            $('#rfc_receptor_ver').html(data.rfc_receptor);
            $('#no_exterior_receptor_ver').html(data.numero_ext_receptor);
            $('#no_interior_receptor_ver').html(data.numero_int_receptor);
            $('#colonia_receptor_ver').html(data.colonia_receptor);
            $('#municipio_receptor_ver').html(data.municipio_receptor);
            $('#estado_receptor_ver').html(data.estado_receptor);
            $('#pais_receptor_ver').html(data.pais_receptor);
            $('#calle_receptor_ver').html(data.calle_receptor);

            $('#fecha_emision_ver').html(data.fecha_emision);
            $('#concepto_ver').html(data.concepto);
            $('#tipo_comprobante_ver').html(data.tipo_comprobante);
            $('#numero_comprobante_ver').html(data.numero_comprobante);
            $('#folio_ver').html(data.folio);
            $('#metodo_pago_ver').html(data.metodo_pago);
            $('#iva_ver').html(data.valor_iva);
            $('#tasa_iva_ver').html(data.tasa_iva);
            $('#subtotal_ver').html(data.subtotal);
            $('#total_ver').html(data.total);
            $('#nombre_cliente_ver').html(data.nombre_cliente);
            //$('#no_certificado_ver').html(data.no_certificado);
            //$('#certificado_ver').html(data.certificado);
            //$('#sello_ver').html(data.sello);
            //
            //$('#uuid_ver').html(data.uuid);
            //$('#no_certificado_sat_ver').html(data.no_certificado_sat);
            //$('#sello_sat_ver').html(data.sello_sat);
            //$('#observaciones_proveedor_ver').html(data.observaciones_proveedor);
            //$('#observaciones_ver').html(data.observaciones);

        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});

$('#elegir_borrar_factura').on('click', function () {

    var table = $('#datos_tabla').DataTable();
    
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../facturas/borrar_factura",
        data: {
            csrfExtranetMorelia: $('input[name=csrfExtranetMorelia]').val(),
            factura: datos_tabla[0]
        }
    })
        .done(function( data ) {
            table.ajax.reload();
            $('#resultado_mensaje').html(data.mensaje);
            $('#modal_resultado').modal('show');
        })
        .fail(function(e) {
            console.log(e.responseText);
        });
});