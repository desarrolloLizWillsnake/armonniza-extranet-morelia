$(document).ready(function() {

    var tabla_principal = $('#datos_tabla').DataTable({
        "drawCallback": function( settings ) {
            var api = this.api();
            if(typeof api !== 'undefined' || typeof api !== null) {
                var info = api.rows( { page:'current' } ).data();                
                if(typeof info[0] !== 'undefined' && typeof info[0] !== null) {
                    var nuevo_csrf = JSON.parse(info[0][5]);
                    $('input[name=csrfExtranetMorelia]').val(nuevo_csrf.csrf_token);
                } else {
                    $('#forma_subir_facturas').hide();
                }
            }
        },
        "responsive": true,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "../facturas/tabla_detalle_contratos",
            "type": "POST",
            "data": function ( d ) {
                d.csrfExtranetMorelia = $('input[name=csrfExtranetMorelia]').val();
                d.usuario = $('input[name=usuario]').val();
            }
        },
        "aaSorting": [],
        "language": {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            },
            select: {
                rows: {
                    _: "Ha seleccionado %d fila(s)",
                    0: "De clic en una fila para seleccionarla",
                    1: "1 fila seleccionada"
                }
            }
        },
        columnDefs: [ {
            "targets": [ 7 ],
            "visible": false,
            "searchable": false
        }, {
            "targets": [ 4 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 2 ) +'</span>';
            }
        }, {
            "targets": [ 5 ],
            "render": function ( data, type, row ) {
                return '<span style="text-align: left; float: left;">$</span>'+'<span style="text-align: right; float: right;">'+ $.number( data, 3 ) +'</span>';
            }
        } ],
        select: {
            style: 'single'
        }
    });

    tabla_principal
        .on( 'select', function ( e, dt, type, indexes ) {
            var rowData = tabla_principal.rows( indexes ).data().toArray();
            datos_tabla = rowData[0][0];
            $('#forma_subir_facturas').show();
        } )
        .on( 'deselect', function ( e, dt, type, indexes ) {
            $('#forma_subir_facturas').hide();
        } );

});

$("#upload_file").on("submit", function(e){
    $(this).append('<input type="hidden" name="numero_compromiso" value="'+datos_tabla+'" /> ');
    return true;
});