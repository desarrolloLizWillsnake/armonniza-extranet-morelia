$(document).on("ready", function() {

    var letras = $.trim($("#username").val().replace(/ /g,'')).length;
    
    if(letras == 12) {
        $( "#forma_registro input[name='tipo_razon_social_hidden']" ).val("moral");
        $( "#forma_registro .nombre_container" ).hide();
        $( "#forma_registro .razon_social_container" ).show();
    } else if(letras == 13) {
        $( "#forma_registro input[name='tipo_razon_social_hidden']" ).val("fisica");
        $( "#forma_registro .razon_social_container" ).hide();
        $( "#forma_registro .nombre_container" ).show();
    } else {
        $( "#forma_registro .razon_social_container" ).val();
        $( "#forma_registro .nombre_container" ).val();
        $( "#forma_registro .razon_social_container" ).hide();
        $( "#forma_registro .nombre_container" ).hide();
    }

});

$( "#username" ).focusout(function() {
    
    var map = {};
    $(":input").each(function() {
        if($(this).val() == null) {
            map[$(this).attr("name")] = null;
        } else {
            map[$(this).attr("name")] = $(this).val();   
        }
    });
    
    var letras = $.trim($("#username").val().replace(/ /g,'')).length;
    
    if(letras == 12) {
        $( "#forma_registro input[name='tipo_razon_social_hidden']" ).val("moral");
        $( "#forma_registro .nombre_container" ).hide();
        $( "#forma_registro .razon_social_container" ).show();
    } else if(letras == 13) {
        $( "#forma_registro input[name='tipo_razon_social_hidden']" ).val("fisica");
        $( "#forma_registro .razon_social_container" ).hide();
        $( "#forma_registro .nombre_container" ).show();
    } else {
        $( "#forma_registro .razon_social_container" ).val();
        $( "#forma_registro .nombre_container" ).val();
        $( "#forma_registro .razon_social_container" ).hide();
        $( "#forma_registro .nombre_container" ).hide();
    }
    
    $.ajax({
        url: '/auth/revisar_usuario',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){
            $( "#forma_registro input[name='csrfExtranetMorelia']" ).val(s.hash);
            $( "#mensaje_usuario" ).html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
    
});

$( "#email" ).focusout(function() {
    
    var map = {};
    $(":input").each(function() {
        map[$(this).attr("name")] = $(this).val();
    });
    
    $.ajax({
        url: '/auth/revisar_correo',
        method: 'POST',
        dataType: 'json',
        data: map,
        success: function(s){            
            $( "#forma_registro input[name='csrfExtranetMorelia']" ).val(s.hash);
            $( "#mensaje_correo" ).html(s.mensaje);
        },
        error: function(e){
            console.log(e.responseText);
        }
    });
    
});

$("#check_terminos").change(function() {
    if(this.checked) {
        $('#register').prop('disabled', false);
    }
    else {
        $('#register').prop('disabled', true);
    }
});